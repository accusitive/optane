package optane;

import optane.module.ModuleManager;

public class Optane {
    public static Optane instance;
    public String client_name;
    public String client_version;
    public String client_codename;
    public ModuleManager moduleManager;

    public Optane() {
        this.client_name = "Optane";
        this.client_codename = "Lithium";
        this.client_version = "0.1";

        this.moduleManager = new ModuleManager();
    }

    public static void load() {
        instance = new Optane();

        System.out.println("[Optane] Loaded");
    }
}
package optane.events;

import optane.eventapi.events.Event;

public class EventPreMotionUpdate implements Event {
    public float rotationYaw;
    public float rotationPitch;
    public double posY;
    public EventPreMotionUpdate(float rotationYaw, float rotationPitch, double posY){
        this.rotationYaw = rotationYaw;
        this.rotationPitch = rotationPitch;
        this.posY = posY;
    }
}

package optane.events;

import net.minecraft.client.gui.FontRenderer;
import optane.eventapi.events.Event;

public class EventRender2D implements Event {
    public int scaledWidth;
    public int scaledHeight;
    public FontRenderer fontRenderer;
    public EventRender2D(int scaledWidth, int scaledHeight, FontRenderer fontRenderer){
        this.scaledWidth = scaledWidth;
        this.scaledHeight = scaledHeight;
        this.fontRenderer = fontRenderer;
    }
}

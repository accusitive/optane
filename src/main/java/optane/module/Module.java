package optane.module;

import optane.module.option.Option;

import java.util.ArrayList;

public class Module {
    public enum Category {
        MOVEMENT, COMBAT, PLAYER, OTHER
    }

    public String name;
    public Category category;
    public int bind;
    public ArrayList<Option> options;
    public boolean state;

    public Module(String name, Category category, int bind, ArrayList<Option> options, boolean defaultState) { //Called at minecraft.java line 476
        this.name = name;
        this.category = category;
        this.bind = bind;
        this.options = options;
        this.state = defaultState;

    }

    public void onEnable() {
    }

    public void onDisable() {
    }
    public void onToggle(){}
    public boolean toggle() {
        onToggle();
        if (this.state) {
            this.onDisable();
            this.state = false;

        } else {
            this.onEnable();
            this.state = true;
        }
        return state;
    }

}

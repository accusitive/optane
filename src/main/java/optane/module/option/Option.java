package optane.module.option;

public class Option<T>{
    public String key;
    public T value;
    public String getKey(){
        return key;
    }
    public T getValue(){
        return value;
    }
    public T setValue(T v){
        value = v;
        return v;
    }
}

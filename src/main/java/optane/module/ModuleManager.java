package optane.module;

import optane.module.modules.Hud;
import optane.module.modules.Sprint;

import java.util.HashMap;

public class ModuleManager {
    public HashMap<String, Module> modules;

    public ModuleManager() {
        modules = new HashMap<>();
        modules.put("sprint", new Sprint("Sprint", Module.Category.MOVEMENT, 78, null, true));
        modules.put("hud", new Hud("HUD", Module.Category.OTHER, 0, null, true));

    }

    public void handleKey(int key) {
        System.out.println(String.format("Received key %s,", key));
        modules.forEach((s, module) -> {
            if (module.bind == key) {
                module.toggle();
                System.out.println(String.format("Toggled Module %s", module.name));

            }
        });
    }
}

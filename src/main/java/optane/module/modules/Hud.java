package optane.module.modules;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.IngameGui;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.text.ITextProperties;
import net.minecraft.util.text.Style;
import net.optifine.gui.GuiScreenOF;
import net.optifine.render.RenderUtils;
import net.optifine.util.GuiRect;
import net.optifine.util.GuiUtils;
import optane.Optane;
import optane.events.EventRender2D;
import optane.eventapi.EventManager;
import optane.eventapi.EventTarget;
import optane.module.Module;
import optane.module.option.Option;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class Hud extends Module {
    @EventTarget
    public void onEvent(EventRender2D eventRender2D) {
        MatrixStack matrixStack = new MatrixStack();
        eventRender2D.fontRenderer.drawString(matrixStack, "Optane", 7,7 , 0x000000);

        eventRender2D.fontRenderer.drawString(matrixStack, "Optane", 6,6 , 0xFF6bcaff);

        int y = 0;

            for(Module module : Optane.instance.moduleManager.modules.values()) {
                if(module.state){
                    int width = eventRender2D.scaledWidth;
                    int textWidth = eventRender2D.fontRenderer.getStringWidth(module.name);
                    int x = width-textWidth;
                    eventRender2D.fontRenderer.drawString(matrixStack, module.name, x, y, 0xFF6bcaff);
                    eventRender2D.fontRenderer.drawString(matrixStack, module.name, x+1, y+1, 0xffffff);
//                    drawBox(50,50,150,150);

                    GuiRect guiRect = new GuiRect(100, 100, 1000, 1000);
                    GuiUtils.fill(matrixStack.getLast().getMatrix(), new GuiRect[]{guiRect}, 0xFFFFFF);
                   y+=10;
            }
        };
    }

    public Hud(String name, Category category, int bind, ArrayList<Option> options, boolean defaultState) {
        super(name, category, bind, options, defaultState);
        EventManager.register(this);
    }

    @Override
    public void onEnable() {
        super.onEnable();
        EventManager.register(this);
    }

    @Override
    public void onDisable() {
        super.onDisable();
        EventManager.unregister(this);
    }
    private void drawBox(int x1, int y1, int x2, int y2)
    {
//        ClickGui gui = WurstClient.INSTANCE.getGui();
//        float[] bgColor = gui.getBgColor();
//        float[] acColor = gui.getAcColor();
//        float opacity = gui.getOpacity();
        float[] bgColor = {30,30,30};
        float[] acColor = {0x6b, 0xca, 0xff};
        float opacity = 1f;
        // color
        GL11.glColor4f(bgColor[0], bgColor[1], bgColor[2], opacity);

        // box
        GL11.glBegin(GL11.GL_QUADS);
        {
            GL11.glVertex2i(x1, y1);
            GL11.glVertex2i(x2, y1);
            GL11.glVertex2i(x2, y2);
            GL11.glVertex2i(x1, y2);
        }
        GL11.glEnd();

        // outline positions
        double xi1 = x1 - 0.1;
        double xi2 = x2 + 0.1;
        double yi1 = y1 - 0.1;
        double yi2 = y2 + 0.1;

        // outline
        GL11.glLineWidth(1);
        GL11.glColor4f(acColor[0], acColor[1], acColor[2], 0.5F);
        GL11.glBegin(GL11.GL_LINE_LOOP);
        {
            GL11.glVertex2d(xi1, yi1);
            GL11.glVertex2d(xi2, yi1);
            GL11.glVertex2d(xi2, yi2);
            GL11.glVertex2d(xi1, yi2);
        }
        GL11.glEnd();

        // shadow positions
        xi1 -= 0.9;
        xi2 += 0.9;
        yi1 -= 0.9;
        yi2 += 0.9;

        // top left
        GL11.glBegin(GL11.GL_POLYGON);
        {
            GL11.glColor4f(acColor[0], acColor[1], acColor[2], 0.75F);
            GL11.glVertex2d(x1, y1);
            GL11.glVertex2d(x2, y1);
            GL11.glColor4f(0, 0, 0, 0);
            GL11.glVertex2d(xi2, yi1);
            GL11.glVertex2d(xi1, yi1);
            GL11.glVertex2d(xi1, yi2);
            GL11.glColor4f(acColor[0], acColor[1], acColor[2], 0.75F);
            GL11.glVertex2d(x1, y2);
        }
        GL11.glEnd();

        // bottom right
        GL11.glBegin(GL11.GL_POLYGON);
        {
            GL11.glVertex2d(x2, y2);
            GL11.glVertex2d(x2, y1);
            GL11.glColor4f(0, 0, 0, 0);
            GL11.glVertex2d(xi2, yi1);
            GL11.glVertex2d(xi2, yi2);
            GL11.glVertex2d(xi1, yi2);
            GL11.glColor4f(acColor[0], acColor[1], acColor[2], 0.75F);
            GL11.glVertex2d(x1, y2);
        }
        GL11.glEnd();
    }
}

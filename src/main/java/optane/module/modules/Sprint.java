package optane.module.modules;

import net.minecraft.client.Minecraft;
import optane.events.EventPreMotionUpdate;
import optane.events.EventTick;
import optane.eventapi.EventManager;
import optane.eventapi.EventTarget;
import optane.module.Module;
import optane.module.option.Option;

import java.util.ArrayList;

public class Sprint extends Module {
    @EventTarget
    public void onEvent(EventPreMotionUpdate eventPreMotionUpdate) {
        if (Minecraft.getInstance().player != null) {
            Minecraft.getInstance().player.setSprinting(true);
        }
    }

    public Sprint(String name, Category category, int bind, ArrayList<Option> options, boolean defaultState) {
        super(name, category, bind, options, defaultState);
        EventManager.register(this);
    }

    @Override
    public void onEnable() {
        super.onEnable();
        EventManager.register(this);
    }

    @Override
    public void onDisable() {
        super.onDisable();
        EventManager.unregister(this);
    }
}

package net.minecraft.block;

import java.util.function.Supplier;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityMerger;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public abstract class AbstractChestBlock<E extends TileEntity> extends ContainerBlock
{
    protected final Supplier < TileEntityType <? extends E >> tileEntityType;

    protected AbstractChestBlock(AbstractBlock.Properties p_i298_1_, Supplier < TileEntityType <? extends E >> p_i298_2_)
    {
        super(p_i298_1_);
        this.tileEntityType = p_i298_2_;
    }

    public abstract TileEntityMerger.ICallbackWrapper <? extends ChestTileEntity > combine(BlockState state, World worldIn, BlockPos pos, boolean p_225536_4_);
}

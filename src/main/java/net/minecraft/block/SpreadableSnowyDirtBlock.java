package net.minecraft.block;

import java.util.Random;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.lighting.LightEngine;
import net.minecraft.world.server.ServerWorld;

public abstract class SpreadableSnowyDirtBlock extends SnowyDirtBlock
{
    protected SpreadableSnowyDirtBlock(AbstractBlock.Properties p_i3037_1_)
    {
        super(p_i3037_1_);
    }

    private static boolean func_220257_b(BlockState state, IWorldReader worldReader, BlockPos pos)
    {
        BlockPos blockpos = pos.up();
        BlockState blockstate = worldReader.getBlockState(blockpos);

        if (blockstate.isIn(Blocks.SNOW) && blockstate.get(SnowBlock.LAYERS) == 1)
        {
            return true;
        }
        else if (blockstate.getFluidState().getLevel() == 8)
        {
            return false;
        }
        else
        {
            int i = LightEngine.func_215613_a(worldReader, state, pos, blockstate, blockpos, Direction.UP, blockstate.getOpacity(worldReader, blockpos));
            return i < worldReader.getMaxLightLevel();
        }
    }

    private static boolean func_220256_c(BlockState state, IWorldReader worldReader, BlockPos pos)
    {
        BlockPos blockpos = pos.up();
        return func_220257_b(state, worldReader, pos) && !worldReader.getFluidState(blockpos).isTagged(FluidTags.WATER);
    }

    public void randomTick(BlockState state, ServerWorld worldIn, BlockPos pos, Random random)
    {
        if (!func_220257_b(state, worldIn, pos))
        {
            worldIn.setBlockState(pos, Blocks.DIRT.getDefaultState());
        }
        else
        {
            if (worldIn.getLight(pos.up()) >= 9)
            {
                BlockState blockstate = this.getDefaultState();

                for (int i = 0; i < 4; ++i)
                {
                    BlockPos blockpos = pos.add(random.nextInt(3) - 1, random.nextInt(5) - 3, random.nextInt(3) - 1);

                    if (worldIn.getBlockState(blockpos).isIn(Blocks.DIRT) && func_220256_c(blockstate, worldIn, blockpos))
                    {
                        worldIn.setBlockState(blockpos, blockstate.with(SNOWY, Boolean.valueOf(worldIn.getBlockState(blockpos.up()).isIn(Blocks.SNOW))));
                    }
                }
            }
        }
    }
}

package net.minecraft.block;

import net.minecraft.potion.Effect;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.IBlockReader;

public class FlowerBlock extends BushBlock
{
    protected static final VoxelShape SHAPE = Block.makeCuboidShape(5.0D, 0.0D, 5.0D, 11.0D, 10.0D, 11.0D);
    private final Effect stewEffect;
    private final int stewEffectDuration;

    public FlowerBlock(Effect p_i3543_1_, int p_i3543_2_, AbstractBlock.Properties p_i3543_3_)
    {
        super(p_i3543_3_);
        this.stewEffect = p_i3543_1_;

        if (p_i3543_1_.isInstant())
        {
            this.stewEffectDuration = p_i3543_2_;
        }
        else
        {
            this.stewEffectDuration = p_i3543_2_ * 20;
        }
    }

    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context)
    {
        Vector3d vector3d = state.getOffset(worldIn, pos);
        return SHAPE.withOffset(vector3d.x, vector3d.y, vector3d.z);
    }

    public AbstractBlock.OffsetType getOffsetType()
    {
        return AbstractBlock.OffsetType.XZ;
    }

    public Effect getStewEffect()
    {
        return this.stewEffect;
    }

    public int getStewEffectDuration()
    {
        return this.stewEffectDuration;
    }
}

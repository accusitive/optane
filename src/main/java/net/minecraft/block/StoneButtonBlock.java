package net.minecraft.block;

import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;

public class StoneButtonBlock extends AbstractButtonBlock
{
    protected StoneButtonBlock(AbstractBlock.Properties p_i2326_1_)
    {
        super(false, p_i2326_1_);
    }

    protected SoundEvent getSoundEvent(boolean isOn)
    {
        return isOn ? SoundEvents.BLOCK_STONE_BUTTON_CLICK_ON : SoundEvents.BLOCK_STONE_BUTTON_CLICK_OFF;
    }
}

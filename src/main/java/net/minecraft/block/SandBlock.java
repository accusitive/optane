package net.minecraft.block;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;

public class SandBlock extends FallingBlock
{
    private final int dustColor;

    public SandBlock(int p_i420_1_, AbstractBlock.Properties p_i420_2_)
    {
        super(p_i420_2_);
        this.dustColor = p_i420_1_;
    }

    public int getDustColor(BlockState state, IBlockReader p_189876_2_, BlockPos p_189876_3_)
    {
        return this.dustColor;
    }
}

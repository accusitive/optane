package net.minecraft.block;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;

public class GravelBlock extends FallingBlock
{
    public GravelBlock(AbstractBlock.Properties p_i1345_1_)
    {
        super(p_i1345_1_);
    }

    public int getDustColor(BlockState state, IBlockReader p_189876_2_, BlockPos p_189876_3_)
    {
        return -8356741;
    }
}

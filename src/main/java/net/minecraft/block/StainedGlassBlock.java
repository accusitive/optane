package net.minecraft.block;

import net.minecraft.item.DyeColor;

public class StainedGlassBlock extends AbstractGlassBlock implements IBeaconBeamColorProvider
{
    private final DyeColor color;

    public StainedGlassBlock(DyeColor worldIn, AbstractBlock.Properties shooter)
    {
        super(shooter);
        this.color = worldIn;
    }

    public DyeColor getColor()
    {
        return this.color;
    }
}

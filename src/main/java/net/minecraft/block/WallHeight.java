package net.minecraft.block;

import net.minecraft.util.IStringSerializable;

public enum WallHeight implements IStringSerializable
{
    NONE("none"),
    LOW("low"),
    TALL("tall");

    private final String field_235922_d_;

    private WallHeight(String p_i2211_3_)
    {
        this.field_235922_d_ = p_i2211_3_;
    }

    public String toString()
    {
        return this.getName1();
    }

    public String getName1()
    {
        return this.field_235922_d_;
    }
}

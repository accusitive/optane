package net.minecraft.entity.ai.goal;

import java.util.function.Predicate;
import javax.annotation.Nullable;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.passive.TameableEntity;

public class NonTamedTargetGoal<T extends LivingEntity> extends NearestAttackableTargetGoal<T>
{
    private final TameableEntity tameable;

    public NonTamedTargetGoal(TameableEntity p_i2642_1_, Class<T> p_i2642_2_, boolean p_i2642_3_, @Nullable Predicate<LivingEntity> p_i2642_4_)
    {
        super(p_i2642_1_, p_i2642_2_, 10, p_i2642_3_, false, p_i2642_4_);
        this.tameable = p_i2642_1_;
    }

    public boolean shouldExecute()
    {
        return !this.tameable.isTamed() && super.shouldExecute();
    }

    public boolean shouldContinueExecuting()
    {
        return this.targetEntitySelector != null ? this.targetEntitySelector.canTarget(this.goalOwner, this.nearestTarget) : super.shouldContinueExecuting();
    }
}

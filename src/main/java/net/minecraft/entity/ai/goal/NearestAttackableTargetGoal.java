package net.minecraft.entity.ai.goal;

import java.util.EnumSet;
import java.util.function.Predicate;
import javax.annotation.Nullable;
import net.minecraft.entity.EntityPredicate;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;

public class NearestAttackableTargetGoal<T extends LivingEntity> extends TargetGoal
{
    protected final Class<T> targetClass;
    protected final int targetChance;
    protected LivingEntity nearestTarget;
    protected EntityPredicate targetEntitySelector;

    public NearestAttackableTargetGoal(MobEntity p_i3884_1_, Class<T> p_i3884_2_, boolean p_i3884_3_)
    {
        this(p_i3884_1_, p_i3884_2_, p_i3884_3_, false);
    }

    public NearestAttackableTargetGoal(MobEntity p_i3885_1_, Class<T> p_i3885_2_, boolean p_i3885_3_, boolean p_i3885_4_)
    {
        this(p_i3885_1_, p_i3885_2_, 10, p_i3885_3_, p_i3885_4_, (Predicate<LivingEntity>)null);
    }

    public NearestAttackableTargetGoal(MobEntity p_i3886_1_, Class<T> p_i3886_2_, int p_i3886_3_, boolean p_i3886_4_, boolean p_i3886_5_, @Nullable Predicate<LivingEntity> p_i3886_6_)
    {
        super(p_i3886_1_, p_i3886_4_, p_i3886_5_);
        this.targetClass = p_i3886_2_;
        this.targetChance = p_i3886_3_;
        this.setMutexFlags(EnumSet.of(Goal.Flag.TARGET));
        this.targetEntitySelector = (new EntityPredicate()).setDistance(this.getTargetDistance()).setCustomPredicate(p_i3886_6_);
    }

    public boolean shouldExecute()
    {
        if (this.targetChance > 0 && this.goalOwner.getRNG().nextInt(this.targetChance) != 0)
        {
            return false;
        }
        else
        {
            this.findNearestTarget();
            return this.nearestTarget != null;
        }
    }

    protected AxisAlignedBB getTargetableArea(double targetDistance)
    {
        return this.goalOwner.getBoundingBox().grow(targetDistance, 4.0D, targetDistance);
    }

    protected void findNearestTarget()
    {
        if (this.targetClass != PlayerEntity.class && this.targetClass != ServerPlayerEntity.class)
        {
            this.nearestTarget = this.goalOwner.world.func_225318_b(this.targetClass, this.targetEntitySelector, this.goalOwner, this.goalOwner.getPosX(), this.goalOwner.getPosYEye(), this.goalOwner.getPosZ(), this.getTargetableArea(this.getTargetDistance()));
        }
        else
        {
            this.nearestTarget = this.goalOwner.world.getClosestPlayer(this.targetEntitySelector, this.goalOwner, this.goalOwner.getPosX(), this.goalOwner.getPosYEye(), this.goalOwner.getPosZ());
        }
    }

    public void startExecuting()
    {
        this.goalOwner.setAttackTarget(this.nearestTarget);
        super.startExecuting();
    }

    public void func_234054_a_(@Nullable LivingEntity p_234054_1_)
    {
        this.nearestTarget = p_234054_1_;
    }
}

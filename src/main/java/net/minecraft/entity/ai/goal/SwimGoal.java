package net.minecraft.entity.ai.goal;

import java.util.EnumSet;
import net.minecraft.entity.MobEntity;
import net.minecraft.tags.FluidTags;

public class SwimGoal extends Goal
{
    private final MobEntity entity;

    public SwimGoal(MobEntity p_i2484_1_)
    {
        this.entity = p_i2484_1_;
        this.setMutexFlags(EnumSet.of(Goal.Flag.JUMP));
        p_i2484_1_.getNavigator().setCanSwim(true);
    }

    public boolean shouldExecute()
    {
        return this.entity.isInWater() && this.entity.getFluidLevel(FluidTags.WATER) > this.entity.SwimThreshold() || this.entity.isInLava();
    }

    public void tick()
    {
        if (this.entity.getRNG().nextFloat() < 0.8F)
        {
            this.entity.getJumpController().setJumping();
        }
    }
}

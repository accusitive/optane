package net.minecraft.entity.ai.goal;

import java.util.EnumSet;
import net.minecraft.block.BlockState;
import net.minecraft.block.LeavesBlock;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.pathfinding.FlyingPathNavigator;
import net.minecraft.pathfinding.GroundPathNavigator;
import net.minecraft.pathfinding.PathNavigator;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.pathfinding.WalkNodeProcessor;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;

public class FollowOwnerGoal extends Goal
{
    private final TameableEntity tameable;
    private LivingEntity owner;
    private final IWorldReader world;
    private final double followSpeed;
    private final PathNavigator navigator;
    private int timeToRecalcPath;
    private final float maxDist;
    private final float minDist;
    private float oldWaterCost;
    private final boolean teleportToLeaves;

    public FollowOwnerGoal(TameableEntity p_i2485_1_, double p_i2485_2_, float p_i2485_4_, float p_i2485_5_, boolean p_i2485_6_)
    {
        this.tameable = p_i2485_1_;
        this.world = p_i2485_1_.world;
        this.followSpeed = p_i2485_2_;
        this.navigator = p_i2485_1_.getNavigator();
        this.minDist = p_i2485_4_;
        this.maxDist = p_i2485_5_;
        this.teleportToLeaves = p_i2485_6_;
        this.setMutexFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));

        if (!(p_i2485_1_.getNavigator() instanceof GroundPathNavigator) && !(p_i2485_1_.getNavigator() instanceof FlyingPathNavigator))
        {
            throw new IllegalArgumentException("Unsupported mob type for FollowOwnerGoal");
        }
    }

    public boolean shouldExecute()
    {
        LivingEntity livingentity = this.tameable.getOwner();

        if (livingentity == null)
        {
            return false;
        }
        else if (livingentity.isSpectator())
        {
            return false;
        }
        else if (this.tameable.func_233685_eM_())
        {
            return false;
        }
        else if (this.tameable.getDistanceSq(livingentity) < (double)(this.minDist * this.minDist))
        {
            return false;
        }
        else
        {
            this.owner = livingentity;
            return true;
        }
    }

    public boolean shouldContinueExecuting()
    {
        if (this.navigator.noPath())
        {
            return false;
        }
        else if (this.tameable.func_233685_eM_())
        {
            return false;
        }
        else
        {
            return !(this.tameable.getDistanceSq(this.owner) <= (double)(this.maxDist * this.maxDist));
        }
    }

    public void startExecuting()
    {
        this.timeToRecalcPath = 0;
        this.oldWaterCost = this.tameable.getPathPriority(PathNodeType.WATER);
        this.tameable.setPathPriority(PathNodeType.WATER, 0.0F);
    }

    public void resetTask()
    {
        this.owner = null;
        this.navigator.clearPath();
        this.tameable.setPathPriority(PathNodeType.WATER, this.oldWaterCost);
    }

    public void tick()
    {
        this.tameable.getLookController().setLookPositionWithEntity(this.owner, 10.0F, (float)this.tameable.getVerticalFaceSpeed());

        if (--this.timeToRecalcPath <= 0)
        {
            this.timeToRecalcPath = 10;

            if (!this.tameable.getLeashed() && !this.tameable.isPassenger())
            {
                if (this.tameable.getDistanceSq(this.owner) >= 144.0D)
                {
                    this.tryToTeleportNearEntity();
                }
                else
                {
                    this.navigator.tryMoveToEntityLiving(this.owner, this.followSpeed);
                }
            }
        }
    }

    private void tryToTeleportNearEntity()
    {
        BlockPos blockpos = this.owner.getBlockPos();

        for (int i = 0; i < 10; ++i)
        {
            int j = this.getRandomNumber(-3, 3);
            int k = this.getRandomNumber(-1, 1);
            int l = this.getRandomNumber(-3, 3);
            boolean flag = this.tryToTeleportToLocation(blockpos.getX() + j, blockpos.getY() + k, blockpos.getZ() + l);

            if (flag)
            {
                return;
            }
        }
    }

    private boolean tryToTeleportToLocation(int xIn, int yIn, int zIn)
    {
        if (Math.abs((double)xIn - this.owner.getPosX()) < 2.0D && Math.abs((double)zIn - this.owner.getPosZ()) < 2.0D)
        {
            return false;
        }
        else if (!this.isTeleportFriendlyBlock(new BlockPos(xIn, yIn, zIn)))
        {
            return false;
        }
        else
        {
            this.tameable.setLocationAndAngles((double)xIn + 0.5D, (double)yIn, (double)zIn + 0.5D, this.tameable.rotationYaw, this.tameable.rotationPitch);
            this.navigator.clearPath();
            return true;
        }
    }

    private boolean isTeleportFriendlyBlock(BlockPos posIn)
    {
        PathNodeType pathnodetype = WalkNodeProcessor.func_237231_a_(this.world, posIn.func_239590_i_());

        if (pathnodetype != PathNodeType.WALKABLE)
        {
            return false;
        }
        else
        {
            BlockState blockstate = this.world.getBlockState(posIn.down());

            if (!this.teleportToLeaves && blockstate.getBlock() instanceof LeavesBlock)
            {
                return false;
            }
            else
            {
                BlockPos blockpos = posIn.subtract(this.tameable.getBlockPos());
                return this.world.hasNoCollisions(this.tameable, this.tameable.getBoundingBox().offset(blockpos));
            }
        }
    }

    private int getRandomNumber(int minIn, int maxIn)
    {
        return this.tameable.getRNG().nextInt(maxIn - minIn + 1) + minIn;
    }
}

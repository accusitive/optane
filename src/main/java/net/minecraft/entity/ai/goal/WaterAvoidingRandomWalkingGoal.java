package net.minecraft.entity.ai.goal;

import javax.annotation.Nullable;
import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.util.math.vector.Vector3d;

public class WaterAvoidingRandomWalkingGoal extends RandomWalkingGoal
{
    protected final float probability;

    public WaterAvoidingRandomWalkingGoal(CreatureEntity p_i4419_1_, double p_i4419_2_)
    {
        this(p_i4419_1_, p_i4419_2_, 0.001F);
    }

    public WaterAvoidingRandomWalkingGoal(CreatureEntity p_i4420_1_, double p_i4420_2_, float p_i4420_4_)
    {
        super(p_i4420_1_, p_i4420_2_);
        this.probability = p_i4420_4_;
    }

    @Nullable
    protected Vector3d getPosition()
    {
        if (this.creature.isInWaterOrBubbleColumn())
        {
            Vector3d vector3d = RandomPositionGenerator.getLandPos(this.creature, 15, 7);
            return vector3d == null ? super.getPosition() : vector3d;
        }
        else
        {
            return this.creature.getRNG().nextFloat() >= this.probability ? RandomPositionGenerator.getLandPos(this.creature, 10, 7) : super.getPosition();
        }
    }
}

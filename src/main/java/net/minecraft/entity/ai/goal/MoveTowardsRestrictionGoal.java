package net.minecraft.entity.ai.goal;

import java.util.EnumSet;
import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.util.math.vector.Vector3d;

public class MoveTowardsRestrictionGoal extends Goal
{
    private final CreatureEntity creature;
    private double movePosX;
    private double movePosY;
    private double movePosZ;
    private final double movementSpeed;

    public MoveTowardsRestrictionGoal(CreatureEntity p_i4009_1_, double p_i4009_2_)
    {
        this.creature = p_i4009_1_;
        this.movementSpeed = p_i4009_2_;
        this.setMutexFlags(EnumSet.of(Goal.Flag.MOVE));
    }

    public boolean shouldExecute()
    {
        if (this.creature.isWithinHomeDistanceCurrentPosition())
        {
            return false;
        }
        else
        {
            Vector3d vector3d = RandomPositionGenerator.findRandomTargetBlockTowards(this.creature, 16, 7, Vector3d.fromBlockPosMiddleBottom(this.creature.getHomePosition()));

            if (vector3d == null)
            {
                return false;
            }
            else
            {
                this.movePosX = vector3d.x;
                this.movePosY = vector3d.y;
                this.movePosZ = vector3d.z;
                return true;
            }
        }
    }

    public boolean shouldContinueExecuting()
    {
        return !this.creature.getNavigator().noPath();
    }

    public void startExecuting()
    {
        this.creature.getNavigator().tryMoveToXYZ(this.movePosX, this.movePosY, this.movePosZ, this.movementSpeed);
    }
}

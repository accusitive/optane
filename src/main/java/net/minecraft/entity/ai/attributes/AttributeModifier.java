package net.minecraft.entity.ai.attributes;

import io.netty.util.internal.ThreadLocalRandom;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Supplier;
import javax.annotation.Nullable;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.MathHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AttributeModifier
{
    private static final Logger field_233799_a_ = LogManager.getLogger();
    private final double amount;
    private final AttributeModifier.Operation operation;
    private final Supplier<String> name;
    private final UUID id;

    public AttributeModifier(String p_i4218_1_, double p_i4218_2_, AttributeModifier.Operation p_i4218_4_)
    {
        this(MathHelper.getRandomUUID(ThreadLocalRandom.current()), () ->
        {
            return p_i4218_1_;
        }, p_i4218_2_, p_i4218_4_);
    }

    public AttributeModifier(UUID p_i4219_1_, String p_i4219_2_, double p_i4219_3_, AttributeModifier.Operation p_i4219_5_)
    {
        this(p_i4219_1_, () ->
        {
            return p_i4219_2_;
        }, p_i4219_3_, p_i4219_5_);
    }

    public AttributeModifier(UUID p_i4220_1_, Supplier<String> p_i4220_2_, double p_i4220_3_, AttributeModifier.Operation p_i4220_5_)
    {
        this.id = p_i4220_1_;
        this.name = p_i4220_2_;
        this.amount = p_i4220_3_;
        this.operation = p_i4220_5_;
    }

    public UUID getID()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name.get();
    }

    public AttributeModifier.Operation getOperation()
    {
        return this.operation;
    }

    public double getAmount()
    {
        return this.amount;
    }

    public boolean equals(Object p_equals_1_)
    {
        if (this == p_equals_1_)
        {
            return true;
        }
        else if (p_equals_1_ != null && this.getClass() == p_equals_1_.getClass())
        {
            AttributeModifier attributemodifier = (AttributeModifier)p_equals_1_;
            return Objects.equals(this.id, attributemodifier.id);
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return this.id.hashCode();
    }

    public String toString()
    {
        return "AttributeModifier{amount=" + this.amount + ", operation=" + this.operation + ", name='" + (String)this.name.get() + '\'' + ", id=" + this.id + '}';
    }

    public CompoundNBT func_233801_e_()
    {
        CompoundNBT compoundnbt = new CompoundNBT();
        compoundnbt.putString("Name", this.getName());
        compoundnbt.putDouble("Amount", this.amount);
        compoundnbt.putInt("Operation", this.operation.getId());
        compoundnbt.putUniqueId("UUID", this.id);
        return compoundnbt;
    }

    @Nullable
    public static AttributeModifier func_233800_a_(CompoundNBT p_233800_0_)
    {
        try
        {
            UUID uuid = p_233800_0_.getUniqueId("UUID");
            AttributeModifier.Operation attributemodifier$operation = AttributeModifier.Operation.byId(p_233800_0_.getInt("Operation"));
            return new AttributeModifier(uuid, p_233800_0_.getString("Name"), p_233800_0_.getDouble("Amount"), attributemodifier$operation);
        }
        catch (Exception exception)
        {
            field_233799_a_.warn("Unable to create attribute: {}", (Object)exception.getMessage());
            return null;
        }
    }

    public static enum Operation
    {
        ADDITION(0),
        MULTIPLY_BASE(1),
        MULTIPLY_TOTAL(2);

        private static final AttributeModifier.Operation[] VALUES = new AttributeModifier.Operation[]{ADDITION, MULTIPLY_BASE, MULTIPLY_TOTAL};
        private final int id;

        private Operation(int p_i2224_3_)
        {
            this.id = p_i2224_3_;
        }

        public int getId()
        {
            return this.id;
        }

        public static AttributeModifier.Operation byId(int id)
        {
            if (id >= 0 && id < VALUES.length)
            {
                return VALUES[id];
            }
            else
            {
                throw new IllegalArgumentException("No operation with value " + id);
            }
        }
    }
}

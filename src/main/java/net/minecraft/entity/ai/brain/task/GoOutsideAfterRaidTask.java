package net.minecraft.entity.ai.brain.task;

import net.minecraft.entity.LivingEntity;
import net.minecraft.world.raid.Raid;
import net.minecraft.world.server.ServerWorld;

public class GoOutsideAfterRaidTask extends MoveToSkylightTask
{
    public GoOutsideAfterRaidTask(float p_i1738_1_)
    {
        super(p_i1738_1_);
    }

    protected boolean shouldExecute(ServerWorld worldIn, LivingEntity owner)
    {
        Raid raid = worldIn.findRaid(owner.getBlockPos());
        return raid != null && raid.isVictory() && super.shouldExecute(worldIn, owner);
    }
}

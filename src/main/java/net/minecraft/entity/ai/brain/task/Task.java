package net.minecraft.entity.ai.brain.task;

import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.brain.memory.MemoryModuleStatus;
import net.minecraft.entity.ai.brain.memory.MemoryModuleType;
import net.minecraft.world.server.ServerWorld;

public abstract class Task<E extends LivingEntity>
{
    protected final Map < MemoryModuleType<?>, MemoryModuleStatus > requiredMemoryState;
    private Task.Status status = Task.Status.STOPPED;
    private long stopTime;
    private final int durationMin;
    private final int durationMax;

    public Task(Map < MemoryModuleType<?>, MemoryModuleStatus > p_i2717_1_)
    {
        this(p_i2717_1_, 60);
    }

    public Task(Map < MemoryModuleType<?>, MemoryModuleStatus > p_i2718_1_, int p_i2718_2_)
    {
        this(p_i2718_1_, p_i2718_2_, p_i2718_2_);
    }

    public Task(Map < MemoryModuleType<?>, MemoryModuleStatus > p_i2719_1_, int p_i2719_2_, int p_i2719_3_)
    {
        this.durationMin = p_i2719_2_;
        this.durationMax = p_i2719_3_;
        this.requiredMemoryState = p_i2719_1_;
    }

    public Task.Status getStatus()
    {
        return this.status;
    }

    public final boolean start(ServerWorld worldIn, E owner, long gameTime)
    {
        if (this.hasRequiredMemories(owner) && this.shouldExecute(worldIn, owner))
        {
            this.status = Task.Status.RUNNING;
            int i = this.durationMin + worldIn.getRandom().nextInt(this.durationMax + 1 - this.durationMin);
            this.stopTime = gameTime + (long)i;
            this.startExecuting(worldIn, owner, gameTime);
            return true;
        }
        else
        {
            return false;
        }
    }

    protected void startExecuting(ServerWorld worldIn, E entityIn, long gameTimeIn)
    {
    }

    public final void tick(ServerWorld worldIn, E entityIn, long gameTime)
    {
        if (!this.isTimedOut(gameTime) && this.shouldContinueExecuting(worldIn, entityIn, gameTime))
        {
            this.updateTask(worldIn, entityIn, gameTime);
        }
        else
        {
            this.stop(worldIn, entityIn, gameTime);
        }
    }

    protected void updateTask(ServerWorld worldIn, E owner, long gameTime)
    {
    }

    public final void stop(ServerWorld worldIn, E entityIn, long gameTimeIn)
    {
        this.status = Task.Status.STOPPED;
        this.resetTask(worldIn, entityIn, gameTimeIn);
    }

    protected void resetTask(ServerWorld worldIn, E entityIn, long gameTimeIn)
    {
    }

    protected boolean shouldContinueExecuting(ServerWorld worldIn, E entityIn, long gameTimeIn)
    {
        return false;
    }

    protected boolean isTimedOut(long gameTime)
    {
        return gameTime > this.stopTime;
    }

    protected boolean shouldExecute(ServerWorld worldIn, E owner)
    {
        return true;
    }

    public String toString()
    {
        return this.getClass().getSimpleName();
    }

    private boolean hasRequiredMemories(E owner)
    {
        for (Entry < MemoryModuleType<?>, MemoryModuleStatus > entry : this.requiredMemoryState.entrySet())
        {
            MemoryModuleType<?> memorymoduletype = entry.getKey();
            MemoryModuleStatus memorymodulestatus = entry.getValue();

            if (!owner.getBrain().hasMemory(memorymoduletype, memorymodulestatus))
            {
                return false;
            }
        }

        return true;
    }

    public static enum Status
    {
        STOPPED,
        RUNNING;
    }
}

package net.minecraft.entity.ai.brain.memory;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPosWrapper;
import net.minecraft.util.math.IPosWrapper;
import net.minecraft.util.math.vector.Vector3d;

public class WalkTarget
{
    private final IPosWrapper target;
    private final float speed;
    private final int distance;

    public WalkTarget(BlockPos p_i4274_1_, float p_i4274_2_, int p_i4274_3_)
    {
        this(new BlockPosWrapper(p_i4274_1_), p_i4274_2_, p_i4274_3_);
    }

    public WalkTarget(Vector3d p_i4275_1_, float p_i4275_2_, int p_i4275_3_)
    {
        this(new BlockPosWrapper(new BlockPos(p_i4275_1_)), p_i4275_2_, p_i4275_3_);
    }

    public WalkTarget(IPosWrapper p_i4276_1_, float p_i4276_2_, int p_i4276_3_)
    {
        this.target = p_i4276_1_;
        this.speed = p_i4276_2_;
        this.distance = p_i4276_3_;
    }

    public IPosWrapper getTarget()
    {
        return this.target;
    }

    public float getSpeed()
    {
        return this.speed;
    }

    public int getDistance()
    {
        return this.distance;
    }
}

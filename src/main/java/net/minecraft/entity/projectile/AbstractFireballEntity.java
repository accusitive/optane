package net.minecraft.entity.projectile;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.IRendersAsItem;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.Util;
import net.minecraft.world.World;

public abstract class AbstractFireballEntity extends DamagingProjectileEntity implements IRendersAsItem
{
    private static final DataParameter<ItemStack> STACK = EntityDataManager.createKey(AbstractFireballEntity.class, DataSerializers.ITEMSTACK);

    public AbstractFireballEntity(EntityType <? extends AbstractFireballEntity > p_i3497_1_, World p_i3497_2_)
    {
        super(p_i3497_1_, p_i3497_2_);
    }

    public AbstractFireballEntity(EntityType <? extends AbstractFireballEntity > p_i3498_1_, double p_i3498_2_, double p_i3498_4_, double p_i3498_6_, double p_i3498_8_, double p_i3498_10_, double p_i3498_12_, World p_i3498_14_)
    {
        super(p_i3498_1_, p_i3498_2_, p_i3498_4_, p_i3498_6_, p_i3498_8_, p_i3498_10_, p_i3498_12_, p_i3498_14_);
    }

    public AbstractFireballEntity(EntityType <? extends AbstractFireballEntity > p_i3499_1_, LivingEntity p_i3499_2_, double p_i3499_3_, double p_i3499_5_, double p_i3499_7_, World p_i3499_9_)
    {
        super(p_i3499_1_, p_i3499_2_, p_i3499_3_, p_i3499_5_, p_i3499_7_, p_i3499_9_);
    }

    public void setStack(ItemStack p_213898_1_)
    {
        if (p_213898_1_.getItem() != Items.FIRE_CHARGE || p_213898_1_.hasTag())
        {
            this.getDataManager().set(STACK, Util.make(p_213898_1_.copy(), (p_213897_0_) ->
            {
                p_213897_0_.setCount(1);
            }));
        }
    }

    protected ItemStack getStack()
    {
        return this.getDataManager().get(STACK);
    }

    public ItemStack getItem()
    {
        ItemStack itemstack = this.getStack();
        return itemstack.isEmpty() ? new ItemStack(Items.FIRE_CHARGE) : itemstack;
    }

    protected void registerData()
    {
        this.getDataManager().register(STACK, ItemStack.EMPTY);
    }

    public void writeAdditional(CompoundNBT compound)
    {
        super.writeAdditional(compound);
        ItemStack itemstack = this.getStack();

        if (!itemstack.isEmpty())
        {
            compound.put("Item", itemstack.write(new CompoundNBT()));
        }
    }

    public void readAdditional(CompoundNBT compound)
    {
        super.readAdditional(compound);
        ItemStack itemstack = ItemStack.read(compound.getCompound("Item"));
        this.setStack(itemstack);
    }
}

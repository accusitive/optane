package net.minecraft.entity.projectile;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.Explosion;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;

public class FireballEntity extends AbstractFireballEntity
{
    public int explosionPower = 1;

    public FireballEntity(EntityType <? extends FireballEntity > p_i1041_1_, World p_i1041_2_)
    {
        super(p_i1041_1_, p_i1041_2_);
    }

    public FireballEntity(World p_i1042_1_, double p_i1042_2_, double p_i1042_4_, double p_i1042_6_, double p_i1042_8_, double p_i1042_10_, double p_i1042_12_)
    {
        super(EntityType.FIREBALL, p_i1042_2_, p_i1042_4_, p_i1042_6_, p_i1042_8_, p_i1042_10_, p_i1042_12_, p_i1042_1_);
    }

    public FireballEntity(World screen, LivingEntity gameSettingsObj, double manager, double p_i1043_5_, double p_i1043_7_)
    {
        super(EntityType.FIREBALL, gameSettingsObj, manager, p_i1043_5_, p_i1043_7_, screen);
    }

    protected void onImpact(RayTraceResult result)
    {
        super.onImpact(result);

        if (!this.world.isRemote)
        {
            boolean flag = this.world.getGameRules().getBoolean(GameRules.MOB_GRIEFING);
            this.world.createExplosion((Entity)null, this.getPosX(), this.getPosY(), this.getPosZ(), (float)this.explosionPower, flag, flag ? Explosion.Mode.DESTROY : Explosion.Mode.NONE);
            this.remove();
        }
    }

    protected void onEntityHit(EntityRayTraceResult p_213868_1_)
    {
        super.onEntityHit(p_213868_1_);

        if (!this.world.isRemote)
        {
            Entity entity = p_213868_1_.getEntity();
            Entity entity1 = this.getShooter();
            entity.attackEntityFrom(DamageSource.func_233547_a_(this, entity1), 6.0F);

            if (entity1 instanceof LivingEntity)
            {
                this.applyEnchantments((LivingEntity)entity1, entity);
            }
        }
    }

    public void writeAdditional(CompoundNBT compound)
    {
        super.writeAdditional(compound);
        compound.putInt("ExplosionPower", this.explosionPower);
    }

    public void readAdditional(CompoundNBT compound)
    {
        super.readAdditional(compound);

        if (compound.contains("ExplosionPower", 99))
        {
            this.explosionPower = compound.getInt("ExplosionPower");
        }
    }
}

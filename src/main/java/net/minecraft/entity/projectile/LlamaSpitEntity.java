package net.minecraft.entity.projectile;

import net.minecraft.block.AbstractBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.passive.horse.LlamaEntity;
import net.minecraft.network.IPacket;
import net.minecraft.network.play.server.SSpawnObjectPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;

public class LlamaSpitEntity extends ProjectileEntity
{
    public LlamaSpitEntity(EntityType <? extends LlamaSpitEntity > xCoord, World p_i1368_2_)
    {
        super(xCoord, p_i1368_2_);
    }

    public LlamaSpitEntity(World p_i1369_1_, LlamaEntity p_i1369_2_)
    {
        this(EntityType.LLAMA_SPIT, p_i1369_1_);
        super.setShooter(p_i1369_2_);
        this.setPosition(p_i1369_2_.getPosX() - (double)(p_i1369_2_.getWidth() + 1.0F) * 0.5D * (double)MathHelper.sin(p_i1369_2_.renderYawOffset * ((float)Math.PI / 180F)), p_i1369_2_.getPosYEye() - (double)0.1F, p_i1369_2_.getPosZ() + (double)(p_i1369_2_.getWidth() + 1.0F) * 0.5D * (double)MathHelper.cos(p_i1369_2_.renderYawOffset * ((float)Math.PI / 180F)));
    }

    public LlamaSpitEntity(World p_i1370_1_, double p_i1370_2_, double p_i1370_4_, double p_i1370_6_, double p_i1370_8_, double p_i1370_10_, double p_i1370_12_)
    {
        this(EntityType.LLAMA_SPIT, p_i1370_1_);
        this.setPosition(p_i1370_2_, p_i1370_4_, p_i1370_6_);

        for (int i = 0; i < 7; ++i)
        {
            double d0 = 0.4D + 0.1D * (double)i;
            p_i1370_1_.addParticle(ParticleTypes.SPIT, p_i1370_2_, p_i1370_4_, p_i1370_6_, p_i1370_8_ * d0, p_i1370_10_, p_i1370_12_ * d0);
        }

        this.setMotion(p_i1370_8_, p_i1370_10_, p_i1370_12_);
    }

    public void tick()
    {
        super.tick();
        Vector3d vector3d = this.getMotion();
        RayTraceResult raytraceresult = ProjectileHelper.func_234618_a_(this, this::canImpact, RayTraceContext.BlockMode.OUTLINE);

        if (raytraceresult != null)
        {
            this.onImpact(raytraceresult);
        }

        double d0 = this.getPosX() + vector3d.x;
        double d1 = this.getPosY() + vector3d.y;
        double d2 = this.getPosZ() + vector3d.z;
        this.func_234617_x_();
        float f = 0.99F;
        float f1 = 0.06F;

        if (this.world.func_234853_a_(this.getBoundingBox()).noneMatch(AbstractBlock.AbstractBlockState::isAir))
        {
            this.remove();
        }
        else if (this.isInWaterOrBubbleColumn())
        {
            this.remove();
        }
        else
        {
            this.setMotion(vector3d.scale((double)0.99F));

            if (!this.hasNoGravity())
            {
                this.setMotion(this.getMotion().add(0.0D, (double) - 0.06F, 0.0D));
            }

            this.setPosition(d0, d1, d2);
        }
    }

    protected void onEntityHit(EntityRayTraceResult p_213868_1_)
    {
        super.onEntityHit(p_213868_1_);
        Entity entity = this.getShooter();

        if (entity instanceof LivingEntity)
        {
            p_213868_1_.getEntity().attackEntityFrom(DamageSource.causeIndirectDamage(this, (LivingEntity)entity).setProjectile(), 1.0F);
        }
    }

    protected void onBlockHit(BlockRayTraceResult p_230299_1_)
    {
        super.onBlockHit(p_230299_1_);

        if (!this.world.isRemote)
        {
            this.remove();
        }
    }

    protected void registerData()
    {
    }

    public IPacket<?> createSpawnPacket()
    {
        return new SSpawnObjectPacket(this);
    }
}

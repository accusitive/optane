package net.minecraft.entity.projectile;

import net.minecraft.block.AbstractFireBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;

public class SmallFireballEntity extends AbstractFireballEntity
{
    public SmallFireballEntity(EntityType <? extends SmallFireballEntity > p_i719_1_, World p_i719_2_)
    {
        super(p_i719_1_, p_i719_2_);
    }

    public SmallFireballEntity(World p_i720_1_, LivingEntity p_i720_2_, double p_i720_3_, double p_i720_5_, double p_i720_7_)
    {
        super(EntityType.SMALL_FIREBALL, p_i720_2_, p_i720_3_, p_i720_5_, p_i720_7_, p_i720_1_);
    }

    public SmallFireballEntity(World p_i721_1_, double p_i721_2_, double p_i721_4_, double p_i721_6_, double p_i721_8_, double p_i721_10_, double p_i721_12_)
    {
        super(EntityType.SMALL_FIREBALL, p_i721_2_, p_i721_4_, p_i721_6_, p_i721_8_, p_i721_10_, p_i721_12_, p_i721_1_);
    }

    protected void onEntityHit(EntityRayTraceResult p_213868_1_)
    {
        super.onEntityHit(p_213868_1_);

        if (!this.world.isRemote)
        {
            Entity entity = p_213868_1_.getEntity();

            if (!entity.func_230279_az_())
            {
                Entity entity1 = this.getShooter();
                int i = entity.getFireTimer();
                entity.setFire(5);
                boolean flag = entity.attackEntityFrom(DamageSource.func_233547_a_(this, entity1), 5.0F);

                if (!flag)
                {
                    entity.func_241209_g_(i);
                }
                else if (entity1 instanceof LivingEntity)
                {
                    this.applyEnchantments((LivingEntity)entity1, entity);
                }
            }
        }
    }

    protected void onBlockHit(BlockRayTraceResult p_230299_1_)
    {
        super.onBlockHit(p_230299_1_);

        if (!this.world.isRemote)
        {
            Entity entity = this.getShooter();

            if (entity == null || !(entity instanceof MobEntity) || this.world.getGameRules().getBoolean(GameRules.MOB_GRIEFING))
            {
                BlockPos blockpos = p_230299_1_.getPos().offset(p_230299_1_.getFace());

                if (this.world.isAirBlock(blockpos))
                {
                    this.world.setBlockState(blockpos, AbstractFireBlock.func_235326_a_(this.world, blockpos));
                }
            }
        }
    }

    protected void onImpact(RayTraceResult result)
    {
        super.onImpact(result);

        if (!this.world.isRemote)
        {
            this.remove();
        }
    }

    public boolean canBeCollidedWith()
    {
        return false;
    }

    public boolean attackEntityFrom(DamageSource source, float amount)
    {
        return false;
    }
}

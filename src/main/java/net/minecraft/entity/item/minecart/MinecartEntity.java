package net.minecraft.entity.item.minecart;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.world.World;

public class MinecartEntity extends AbstractMinecartEntity
{
    public MinecartEntity(EntityType<?> p_i1889_1_, World p_i1889_2_)
    {
        super(p_i1889_1_, p_i1889_2_);
    }

    public MinecartEntity(World p_i1890_1_, double p_i1890_2_, double p_i1890_4_, double p_i1890_6_)
    {
        super(EntityType.MINECART, p_i1890_1_, p_i1890_2_, p_i1890_4_, p_i1890_6_);
    }

    public ActionResultType processInitialInteract(PlayerEntity player, Hand hand)
    {
        if (player.isSecondaryUseActive())
        {
            return ActionResultType.PASS;
        }
        else if (this.isBeingRidden())
        {
            return ActionResultType.PASS;
        }
        else if (!this.world.isRemote)
        {
            return player.startRiding(this) ? ActionResultType.CONSUME : ActionResultType.PASS;
        }
        else
        {
            return ActionResultType.SUCCESS;
        }
    }

    public void onActivatorRailPass(int x, int y, int z, boolean receivingPower)
    {
        if (receivingPower)
        {
            if (this.isBeingRidden())
            {
                this.removePassengers();
            }

            if (this.getRollingAmplitude() == 0)
            {
                this.setRollingDirection(-this.getRollingDirection());
                this.setRollingAmplitude(10);
                this.setDamage(50.0F);
                this.markVelocityChanged();
            }
        }
    }

    public AbstractMinecartEntity.Type getMinecartType()
    {
        return AbstractMinecartEntity.Type.RIDEABLE;
    }
}

package net.minecraft.entity.item.minecart;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.ChestBlock;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ChestContainer;
import net.minecraft.inventory.container.Container;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;

public class ChestMinecartEntity extends ContainerMinecartEntity
{
    public ChestMinecartEntity(EntityType <? extends ChestMinecartEntity > p_i2271_1_, World p_i2271_2_)
    {
        super(p_i2271_1_, p_i2271_2_);
    }

    public ChestMinecartEntity(World p_i2272_1_, double p_i2272_2_, double p_i2272_4_, double p_i2272_6_)
    {
        super(EntityType.CHEST_MINECART, p_i2272_2_, p_i2272_4_, p_i2272_6_, p_i2272_1_);
    }

    public void killMinecart(DamageSource source)
    {
        super.killMinecart(source);

        if (this.world.getGameRules().getBoolean(GameRules.DO_ENTITY_DROPS))
        {
            this.entityDropItem(Blocks.CHEST);
        }
    }

    public int getSizeInventory()
    {
        return 27;
    }

    public AbstractMinecartEntity.Type getMinecartType()
    {
        return AbstractMinecartEntity.Type.CHEST;
    }

    public BlockState getDefaultDisplayTile()
    {
        return Blocks.CHEST.getDefaultState().with(ChestBlock.FACING, Direction.NORTH);
    }

    public int getDefaultDisplayTileOffset()
    {
        return 8;
    }

    public Container createContainer(int id, PlayerInventory playerInventoryIn)
    {
        return ChestContainer.createGeneric9X3(id, playerInventoryIn, this);
    }
}

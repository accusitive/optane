package net.minecraft.entity.item;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ProjectileItemEntity;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.potion.PotionUtils;
import net.minecraft.potion.Potions;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class ExperienceBottleEntity extends ProjectileItemEntity
{
    public ExperienceBottleEntity(EntityType <? extends ExperienceBottleEntity > p_i3137_1_, World p_i3137_2_)
    {
        super(p_i3137_1_, p_i3137_2_);
    }

    public ExperienceBottleEntity(World p_i3138_1_, LivingEntity p_i3138_2_)
    {
        super(EntityType.EXPERIENCE_BOTTLE, p_i3138_2_, p_i3138_1_);
    }

    public ExperienceBottleEntity(World p_i3139_1_, double p_i3139_2_, double p_i3139_4_, double p_i3139_6_)
    {
        super(EntityType.EXPERIENCE_BOTTLE, p_i3139_2_, p_i3139_4_, p_i3139_6_, p_i3139_1_);
    }

    protected Item getDefaultItem()
    {
        return Items.EXPERIENCE_BOTTLE;
    }

    protected float getGravityVelocity()
    {
        return 0.07F;
    }

    protected void onImpact(RayTraceResult result)
    {
        super.onImpact(result);

        if (!this.world.isRemote)
        {
            this.world.playEvent(2002, this.getBlockPos(), PotionUtils.getPotionColor(Potions.WATER));
            int i = 3 + this.world.rand.nextInt(5) + this.world.rand.nextInt(5);

            while (i > 0)
            {
                int j = ExperienceOrbEntity.getXPSplit(i);
                i -= j;
                this.world.addEntity(new ExperienceOrbEntity(this.world, this.getPosX(), this.getPosY(), this.getPosZ(), j));
            }

            this.remove();
        }
    }
}

package net.minecraft.profiler;

public final class DataPoint implements Comparable<DataPoint>
{
    public final double relTime;
    public final double rootRelTime;
    public final long field_223511_c;
    public final String name;

    public DataPoint(String p_i3584_1_, double p_i3584_2_, double p_i3584_4_, long p_i3584_6_)
    {
        this.name = p_i3584_1_;
        this.relTime = p_i3584_2_;
        this.rootRelTime = p_i3584_4_;
        this.field_223511_c = p_i3584_6_;
    }

    public int compareTo(DataPoint p_compareTo_1_)
    {
        if (p_compareTo_1_.relTime < this.relTime)
        {
            return -1;
        }
        else
        {
            return p_compareTo_1_.relTime > this.relTime ? 1 : p_compareTo_1_.name.compareTo(this.name);
        }
    }

    public int getTextColor()
    {
        return (this.name.hashCode() & 11184810) + 4473924;
    }
}

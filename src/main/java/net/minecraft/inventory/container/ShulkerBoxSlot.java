package net.minecraft.inventory.container;

import net.minecraft.block.Block;
import net.minecraft.block.ShulkerBoxBlock;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class ShulkerBoxSlot extends Slot
{
    public ShulkerBoxSlot(IInventory p_i2882_1_, int p_i2882_2_, int p_i2882_3_, int p_i2882_4_)
    {
        super(p_i2882_1_, p_i2882_2_, p_i2882_3_, p_i2882_4_);
    }

    public boolean isItemValid(ItemStack stack)
    {
        return !(Block.getBlockFromItem(stack.getItem()) instanceof ShulkerBoxBlock);
    }
}

package net.minecraft.inventory.container;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.util.IIntArray;

public class FurnaceContainer extends AbstractFurnaceContainer
{
    public FurnaceContainer(int p_i1569_1_, PlayerInventory p_i1569_2_)
    {
        super(ContainerType.FURNACE, IRecipeType.SMELTING, p_i1569_1_, p_i1569_2_);
    }

    public FurnaceContainer(int p_i1570_1_, PlayerInventory p_i1570_2_, IInventory p_i1570_3_, IIntArray p_i1570_4_)
    {
        super(ContainerType.FURNACE, IRecipeType.SMELTING, p_i1570_1_, p_i1570_2_, p_i1570_3_, p_i1570_4_);
    }
}

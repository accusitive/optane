package net.minecraft.inventory.container;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.util.IIntArray;

public class SmokerContainer extends AbstractFurnaceContainer
{
    public SmokerContainer(int p_i2438_1_, PlayerInventory p_i2438_2_)
    {
        super(ContainerType.SMOKER, IRecipeType.SMOKING, p_i2438_1_, p_i2438_2_);
    }

    public SmokerContainer(int p_i2439_1_, PlayerInventory p_i2439_2_, IInventory p_i2439_3_, IIntArray p_i2439_4_)
    {
        super(ContainerType.SMOKER, IRecipeType.SMOKING, p_i2439_1_, p_i2439_2_, p_i2439_3_, p_i2439_4_);
    }
}

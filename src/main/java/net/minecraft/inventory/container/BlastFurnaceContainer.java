package net.minecraft.inventory.container;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.util.IIntArray;

public class BlastFurnaceContainer extends AbstractFurnaceContainer
{
    public BlastFurnaceContainer(int p_i2747_1_, PlayerInventory p_i2747_2_)
    {
        super(ContainerType.BLAST_FURNACE, IRecipeType.BLASTING, p_i2747_1_, p_i2747_2_);
    }

    public BlastFurnaceContainer(int p_i2748_1_, PlayerInventory p_i2748_2_, IInventory p_i2748_3_, IIntArray p_i2748_4_)
    {
        super(ContainerType.BLAST_FURNACE, IRecipeType.BLASTING, p_i2748_1_, p_i2748_2_, p_i2748_3_, p_i2748_4_);
    }
}

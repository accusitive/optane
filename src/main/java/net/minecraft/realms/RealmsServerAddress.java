package net.minecraft.realms;

import net.minecraft.client.multiplayer.ServerAddress;

public class RealmsServerAddress
{
    private final String field_230727_a_;
    private final int field_230728_b_;

    protected RealmsServerAddress(String p_i3253_1_, int p_i3253_2_)
    {
        this.field_230727_a_ = p_i3253_1_;
        this.field_230728_b_ = p_i3253_2_;
    }

    public String func_231412_a_()
    {
        return this.field_230727_a_;
    }

    public int func_231414_b_()
    {
        return this.field_230728_b_;
    }

    public static RealmsServerAddress func_231413_a_(String p_231413_0_)
    {
        ServerAddress serveraddress = ServerAddress.fromString(p_231413_0_);
        return new RealmsServerAddress(serveraddress.getIP(), serveraddress.getPort());
    }
}

package net.minecraft.server.management;

import com.google.gson.JsonObject;
import javax.annotation.Nullable;

public abstract class UserListEntry<T>
{
    @Nullable
    private final T value;

    public UserListEntry(@Nullable T p_i3806_1_)
    {
        this.value = p_i3806_1_;
    }

    @Nullable
    T getValue()
    {
        return this.value;
    }

    boolean hasBanExpired()
    {
        return false;
    }

    protected abstract void onSerialization(JsonObject data);
}

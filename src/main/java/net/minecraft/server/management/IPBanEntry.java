package net.minecraft.server.management;

import com.google.gson.JsonObject;
import java.util.Date;
import javax.annotation.Nullable;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;

public class IPBanEntry extends BanEntry<String>
{
    public IPBanEntry(String server)
    {
        this(server, (Date)null, (String)null, (Date)null, (String)null);
    }

    public IPBanEntry(String serverIn, @Nullable Date p_i2367_2_, @Nullable String p_i2367_3_, @Nullable Date p_i2367_4_, @Nullable String p_i2367_5_)
    {
        super(serverIn, p_i2367_2_, p_i2367_3_, p_i2367_4_, p_i2367_5_);
    }

    public ITextComponent getDisplayName()
    {
        return new StringTextComponent(this.getValue());
    }

    public IPBanEntry(JsonObject p_i2368_1_)
    {
        super(getIPFromJson(p_i2368_1_), p_i2368_1_);
    }

    private static String getIPFromJson(JsonObject json)
    {
        return json.has("ip") ? json.get("ip").getAsString() : null;
    }

    protected void onSerialization(JsonObject data)
    {
        if (this.getValue() != null)
        {
            data.addProperty("ip", this.getValue());
            super.onSerialization(data);
        }
    }
}

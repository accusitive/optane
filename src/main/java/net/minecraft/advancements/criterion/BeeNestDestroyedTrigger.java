package net.minecraft.advancements.criterion;

import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import javax.annotation.Nullable;
import net.minecraft.block.Block;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.ConditionArrayParser;
import net.minecraft.loot.ConditionArraySerializer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;

public class BeeNestDestroyedTrigger extends AbstractCriterionTrigger<BeeNestDestroyedTrigger.Instance>
{
    private static final ResourceLocation ID = new ResourceLocation("bee_nest_destroyed");

    public ResourceLocation getId()
    {
        return ID;
    }

    public BeeNestDestroyedTrigger.Instance func_230241_b_(JsonObject p_230241_1_, EntityPredicate.AndPredicate p_230241_2_, ConditionArrayParser p_230241_3_)
    {
        Block block = func_226221_a_(p_230241_1_);
        ItemPredicate itempredicate = ItemPredicate.deserialize(p_230241_1_.get("item"));
        MinMaxBounds.IntBound minmaxbounds$intbound = MinMaxBounds.IntBound.fromJson(p_230241_1_.get("num_bees_inside"));
        return new BeeNestDestroyedTrigger.Instance(p_230241_2_, block, itempredicate, minmaxbounds$intbound);
    }

    @Nullable
    private static Block func_226221_a_(JsonObject json)
    {
        if (json.has("block"))
        {
            ResourceLocation resourcelocation = new ResourceLocation(JSONUtils.getString(json, "block"));
            return Registry.BLOCK.getValue(resourcelocation).orElseThrow(() ->
            {
                return new JsonSyntaxException("Unknown block type '" + resourcelocation + "'");
            });
        }
        else
        {
            return null;
        }
    }

    public void test(ServerPlayerEntity player, Block blockIn, ItemStack stack, int p_226223_4_)
    {
        this.func_235959_a_(player, (p_226220_3_) ->
        {
            return p_226220_3_.test(blockIn, stack, p_226223_4_);
        });
    }

    public static class Instance extends CriterionInstance
    {
        @Nullable
        private final Block block;
        private final ItemPredicate predicate;
        private final MinMaxBounds.IntBound bound;

        public Instance(EntityPredicate.AndPredicate p_i3896_1_, @Nullable Block p_i3896_2_, ItemPredicate p_i3896_3_, MinMaxBounds.IntBound p_i3896_4_)
        {
            super(BeeNestDestroyedTrigger.ID, p_i3896_1_);
            this.block = p_i3896_2_;
            this.predicate = p_i3896_3_;
            this.bound = p_i3896_4_;
        }

        public static BeeNestDestroyedTrigger.Instance func_226229_a_(Block p_226229_0_, ItemPredicate.Builder p_226229_1_, MinMaxBounds.IntBound p_226229_2_)
        {
            return new BeeNestDestroyedTrigger.Instance(EntityPredicate.AndPredicate.field_234582_a_, p_226229_0_, p_226229_1_.build(), p_226229_2_);
        }

        public boolean test(Block p_226228_1_, ItemStack p_226228_2_, int p_226228_3_)
        {
            if (this.block != null && p_226228_1_ != this.block)
            {
                return false;
            }
            else
            {
                return !this.predicate.test(p_226228_2_) ? false : this.bound.test(p_226228_3_);
            }
        }

        public JsonObject func_230240_a_(ConditionArraySerializer p_230240_1_)
        {
            JsonObject jsonobject = super.func_230240_a_(p_230240_1_);

            if (this.block != null)
            {
                jsonobject.addProperty("block", Registry.BLOCK.getKey(this.block).toString());
            }

            jsonobject.add("item", this.predicate.serialize());
            jsonobject.add("num_bees_inside", this.bound.serialize());
            return jsonobject;
        }
    }
}

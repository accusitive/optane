package net.minecraft.world.gen.carver;

import com.mojang.serialization.Codec;
import java.util.BitSet;
import java.util.Random;
import java.util.function.Function;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunk;

public class ConfiguredCarver<WC extends ICarverConfig>
{
    public static final Codec < ConfiguredCarver<? >> field_236235_a_ = Registry.CARVER.dispatch("name", (p_236236_0_) ->
    {
        return p_236236_0_.carver;
    }, WorldCarver::func_236244_c_);
    public final WorldCarver<WC> carver;
    public final WC config;

    public ConfiguredCarver(WorldCarver<WC> p_i3535_1_, WC p_i3535_2_)
    {
        this.carver = p_i3535_1_;
        this.config = p_i3535_2_;
    }

    public boolean shouldCarve(Random rand, int chunkX, int chunkZ)
    {
        return this.carver.shouldCarve(rand, chunkX, chunkZ, this.config);
    }

    public boolean carveRegion(IChunk chunkIn, Function<BlockPos, Biome> biomePos, Random rand, int seaLevel, int chunkXOffset, int chunkZOffset, int chunkX, int chunkZ, BitSet carvingMask)
    {
        return this.carver.carveRegion(chunkIn, biomePos, rand, seaLevel, chunkXOffset, chunkZOffset, chunkX, chunkZ, carvingMask, this.config);
    }
}

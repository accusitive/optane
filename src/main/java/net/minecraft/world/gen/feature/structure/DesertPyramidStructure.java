package net.minecraft.world.gen.feature.structure;

import com.mojang.serialization.Codec;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.template.TemplateManager;

public class DesertPyramidStructure extends Structure<NoFeatureConfig>
{
    public DesertPyramidStructure(Codec<NoFeatureConfig> usernameIn)
    {
        super(usernameIn);
    }

    public Structure.IStartFactory<NoFeatureConfig> getStartFactory()
    {
        return DesertPyramidStructure.Start::new;
    }

    public static class Start extends StructureStart<NoFeatureConfig>
    {
        public Start(Structure<NoFeatureConfig> p_i909_1_, int p_i909_2_, int p_i909_3_, MutableBoundingBox p_i909_4_, int p_i909_5_, long p_i909_6_)
        {
            super(p_i909_1_, p_i909_2_, p_i909_3_, p_i909_4_, p_i909_5_, p_i909_6_);
        }

        public void func_230364_a_(ChunkGenerator p_230364_1_, TemplateManager p_230364_2_, int p_230364_3_, int p_230364_4_, Biome p_230364_5_, NoFeatureConfig p_230364_6_)
        {
            DesertPyramidPiece desertpyramidpiece = new DesertPyramidPiece(this.rand, p_230364_3_ * 16, p_230364_4_ * 16);
            this.components.add(desertpyramidpiece);
            this.recalculateStructureSize();
        }
    }
}

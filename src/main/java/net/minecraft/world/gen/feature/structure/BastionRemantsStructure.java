package net.minecraft.world.gen.feature.structure;

import com.mojang.serialization.Codec;
import net.minecraft.util.SharedSeedRandom;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.template.TemplateManager;

public class BastionRemantsStructure extends Structure<BastionRemnantConfig>
{
    public BastionRemantsStructure(Codec<BastionRemnantConfig> p_i4173_1_)
    {
        super(p_i4173_1_);
    }

    protected boolean func_230363_a_(ChunkGenerator p_230363_1_, BiomeProvider p_230363_2_, long p_230363_3_, SharedSeedRandom p_230363_5_, int p_230363_6_, int p_230363_7_, Biome p_230363_8_, ChunkPos p_230363_9_, BastionRemnantConfig p_230363_10_)
    {
        return p_230363_5_.nextInt(5) >= 2;
    }

    public Structure.IStartFactory<BastionRemnantConfig> getStartFactory()
    {
        return BastionRemantsStructure.Start::new;
    }

    public static class Start extends MarginedStructureStart<BastionRemnantConfig>
    {
        public Start(Structure<BastionRemnantConfig> p_i3143_1_, int p_i3143_2_, int p_i3143_3_, MutableBoundingBox p_i3143_4_, int p_i3143_5_, long p_i3143_6_)
        {
            super(p_i3143_1_, p_i3143_2_, p_i3143_3_, p_i3143_4_, p_i3143_5_, p_i3143_6_);
        }

        public void func_230364_a_(ChunkGenerator p_230364_1_, TemplateManager p_230364_2_, int p_230364_3_, int p_230364_4_, Biome p_230364_5_, BastionRemnantConfig p_230364_6_)
        {
            BlockPos blockpos = new BlockPos(p_230364_3_ * 16, 33, p_230364_4_ * 16);
            BastionRemnantsPieces.func_236259_a_(p_230364_1_, p_230364_2_, blockpos, this.components, this.rand, p_230364_6_);
            this.recalculateStructureSize();
        }
    }
}

package net.minecraft.world.gen.feature.structure;

import java.util.List;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Rotation;
import net.minecraft.util.SharedSeedRandom;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.jigsaw.JigsawManager;
import net.minecraft.world.gen.feature.jigsaw.JigsawPiece;
import net.minecraft.world.gen.feature.template.TemplateManager;

public class VillagePieces
{
    public static void func_236425_a_()
    {
        PlainsVillagePools.init();
        SnowyVillagePools.init();
        SavannaVillagePools.init();
        DesertVillagePools.init();
        TaigaVillagePools.init();
    }

    public static void addPieces(ChunkGenerator chunkGeneratorIn, TemplateManager templateManagerIn, BlockPos p_214838_2_, List<StructurePiece> structurePieces, SharedSeedRandom sharedSeedRandomIn, VillageConfig villageConfigIn)
    {
        func_236425_a_();
        JigsawManager.func_236823_a_(villageConfigIn.startPool, villageConfigIn.size, VillagePieces.Village::new, chunkGeneratorIn, templateManagerIn, p_214838_2_, structurePieces, sharedSeedRandomIn, true, true);
    }

    public static class Village extends AbstractVillagePiece
    {
        public Village(TemplateManager p_i3589_1_, JigsawPiece p_i3589_2_, BlockPos p_i3589_3_, int p_i3589_4_, Rotation p_i3589_5_, MutableBoundingBox p_i3589_6_)
        {
            super(IStructurePieceType.NVI, p_i3589_1_, p_i3589_2_, p_i3589_3_, p_i3589_4_, p_i3589_5_, p_i3589_6_);
        }

        public Village(TemplateManager p_i3590_1_, CompoundNBT p_i3590_2_)
        {
            super(p_i3590_1_, p_i3590_2_, IStructurePieceType.NVI);
        }
    }
}

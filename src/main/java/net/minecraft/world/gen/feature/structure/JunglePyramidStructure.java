package net.minecraft.world.gen.feature.structure;

import com.mojang.serialization.Codec;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.template.TemplateManager;

public class JunglePyramidStructure extends Structure<NoFeatureConfig>
{
    public JunglePyramidStructure(Codec<NoFeatureConfig> p_i3461_1_)
    {
        super(p_i3461_1_);
    }

    public Structure.IStartFactory<NoFeatureConfig> getStartFactory()
    {
        return JunglePyramidStructure.Start::new;
    }

    public static class Start extends StructureStart<NoFeatureConfig>
    {
        public Start(Structure<NoFeatureConfig> p_i3597_1_, int p_i3597_2_, int p_i3597_3_, MutableBoundingBox p_i3597_4_, int p_i3597_5_, long p_i3597_6_)
        {
            super(p_i3597_1_, p_i3597_2_, p_i3597_3_, p_i3597_4_, p_i3597_5_, p_i3597_6_);
        }

        public void func_230364_a_(ChunkGenerator p_230364_1_, TemplateManager p_230364_2_, int p_230364_3_, int p_230364_4_, Biome p_230364_5_, NoFeatureConfig p_230364_6_)
        {
            JunglePyramidPiece junglepyramidpiece = new JunglePyramidPiece(this.rand, p_230364_3_ * 16, p_230364_4_ * 16);
            this.components.add(junglepyramidpiece);
            this.recalculateStructureSize();
        }
    }
}

package net.minecraft.world.gen.feature.jigsaw;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import it.unimi.dsi.fastutil.objects.ObjectArrays;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.gen.Heightmap;
import net.minecraft.world.gen.feature.template.GravityStructureProcessor;
import net.minecraft.world.gen.feature.template.StructureProcessor;
import net.minecraft.world.gen.feature.template.TemplateManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JigsawPattern
{
    private static final Logger field_236853_d_ = LogManager.getLogger();
    public static final Codec<JigsawPattern> field_236852_a_ = RecordCodecBuilder.create((p_236854_0_) ->
    {
        return p_236854_0_.group(ResourceLocation.field_240908_a_.fieldOf("name").forGetter(JigsawPattern::getName), ResourceLocation.field_240908_a_.fieldOf("fallback").forGetter(JigsawPattern::getFallback), Codec.mapPair(JigsawPiece.field_236847_e_.fieldOf("element"), Codec.INT.fieldOf("weight")).codec().listOf().promotePartial(Util.func_240982_a_("Pool element: ", field_236853_d_::error)).fieldOf("elements").forGetter((p_236857_0_) -> {
            return p_236857_0_.rawTemplates;
        }), JigsawPattern.PlacementBehaviour.field_236858_c_.fieldOf("projection").forGetter((p_236855_0_) -> {
            return p_236855_0_.placementBehaviour;
        })).apply(p_236854_0_, JigsawPattern::new);
    });
    public static final JigsawPattern EMPTY = new JigsawPattern(new ResourceLocation("empty"), new ResourceLocation("empty"), ImmutableList.of(), JigsawPattern.PlacementBehaviour.RIGID);
    public static final JigsawPattern INVALID = new JigsawPattern(new ResourceLocation("invalid"), new ResourceLocation("invalid"), ImmutableList.of(), JigsawPattern.PlacementBehaviour.RIGID);
    private final ResourceLocation name;
    private final ImmutableList<Pair<JigsawPiece, Integer>> rawTemplates;
    private final List<JigsawPiece> jigsawPieces;
    private final ResourceLocation fallback;
    private final JigsawPattern.PlacementBehaviour placementBehaviour;
    private int maxSize = Integer.MIN_VALUE;

    public JigsawPattern(ResourceLocation p_i2795_1_, ResourceLocation p_i2795_2_, List<Pair<JigsawPiece, Integer>> p_i2795_3_, JigsawPattern.PlacementBehaviour p_i2795_4_)
    {
        this.name = p_i2795_1_;
        this.rawTemplates = ImmutableList.copyOf(p_i2795_3_);
        this.jigsawPieces = Lists.newArrayList();

        for (Pair<JigsawPiece, Integer> pair : p_i2795_3_)
        {
            for (int i = 0; i < pair.getSecond(); ++i)
            {
                this.jigsawPieces.add(pair.getFirst().setPlacementBehaviour(p_i2795_4_));
            }
        }

        this.fallback = p_i2795_2_;
        this.placementBehaviour = p_i2795_4_;
    }

    public int getMaxSize(TemplateManager templateManagerIn)
    {
        if (this.maxSize == Integer.MIN_VALUE)
        {
            this.maxSize = this.jigsawPieces.stream().mapToInt((p_236856_1_) ->
            {
                return p_236856_1_.getBoundingBox(templateManagerIn, BlockPos.ZERO, Rotation.NONE).getYSize();
            }).max().orElse(0);
        }

        return this.maxSize;
    }

    public ResourceLocation getFallback()
    {
        return this.fallback;
    }

    public JigsawPiece getRandomPiece(Random rand)
    {
        return this.jigsawPieces.get(rand.nextInt(this.jigsawPieces.size()));
    }

    public List<JigsawPiece> getShuffledPieces(Random rand)
    {
        return ImmutableList.copyOf(ObjectArrays.shuffle(this.jigsawPieces.toArray(new JigsawPiece[0]), rand));
    }

    public ResourceLocation getName()
    {
        return this.name;
    }

    public int getNumberOfPieces()
    {
        return this.jigsawPieces.size();
    }

    public static enum PlacementBehaviour implements IStringSerializable
    {
        TERRAIN_MATCHING("terrain_matching", ImmutableList.of(new GravityStructureProcessor(Heightmap.Type.WORLD_SURFACE_WG, -1))),
        RIGID("rigid", ImmutableList.of());

        public static final Codec<JigsawPattern.PlacementBehaviour> field_236858_c_ = IStringSerializable.func_233023_a_(JigsawPattern.PlacementBehaviour::values, JigsawPattern.PlacementBehaviour::getBehaviour);
        private static final Map<String, JigsawPattern.PlacementBehaviour> BEHAVIOURS = Arrays.stream(values()).collect(Collectors.toMap(JigsawPattern.PlacementBehaviour::getName, (p_214935_0_) -> {
            return p_214935_0_;
        }));
        private final String name;
        private final ImmutableList<StructureProcessor> structureProcessors;

        private PlacementBehaviour(String height, ImmutableList<StructureProcessor> topIn)
        {
            this.name = height;
            this.structureProcessors = topIn;
        }

        public String getName()
        {
            return this.name;
        }

        public static JigsawPattern.PlacementBehaviour getBehaviour(String nameIn)
        {
            return BEHAVIOURS.get(nameIn);
        }

        public ImmutableList<StructureProcessor> getStructureProcessors()
        {
            return this.structureProcessors;
        }

        public String getName1()
        {
            return this.name;
        }
    }
}

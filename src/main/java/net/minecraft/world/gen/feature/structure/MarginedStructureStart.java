package net.minecraft.world.gen.feature.structure;

import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.gen.feature.IFeatureConfig;

public abstract class MarginedStructureStart<C extends IFeatureConfig> extends StructureStart<C>
{
    public MarginedStructureStart(Structure<C> p_i1229_1_, int p_i1229_2_, int p_i1229_3_, MutableBoundingBox p_i1229_4_, int p_i1229_5_, long p_i1229_6_)
    {
        super(p_i1229_1_, p_i1229_2_, p_i1229_3_, p_i1229_4_, p_i1229_5_, p_i1229_6_);
    }

    protected void recalculateStructureSize()
    {
        super.recalculateStructureSize();
        int i = 12;
        this.bounds.minX -= 12;
        this.bounds.minY -= 12;
        this.bounds.minZ -= 12;
        this.bounds.maxX += 12;
        this.bounds.maxY += 12;
        this.bounds.maxZ += 12;
    }
}

package net.minecraft.world.gen.placement;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

public class ChanceRangeConfig implements IPlacementConfig
{
    public static final Codec<ChanceRangeConfig> field_236459_a_ = RecordCodecBuilder.create((p_236461_0_) ->
    {
        return p_236461_0_.group(Codec.FLOAT.fieldOf("chance").forGetter((p_236464_0_) -> {
            return p_236464_0_.chance;
        }), Codec.INT.fieldOf("bottom_offset").withDefault(0).forGetter((p_236463_0_) -> {
            return p_236463_0_.bottomOffset;
        }), Codec.INT.fieldOf("top_offset").withDefault(0).forGetter((p_236462_0_) -> {
            return p_236462_0_.topOffset;
        }), Codec.INT.fieldOf("top").withDefault(0).forGetter((p_236460_0_) -> {
            return p_236460_0_.top;
        })).apply(p_236461_0_, ChanceRangeConfig::new);
    });
    public final float chance;
    public final int bottomOffset;
    public final int topOffset;
    public final int top;

    public ChanceRangeConfig(float p_i1394_1_, int p_i1394_2_, int p_i1394_3_, int p_i1394_4_)
    {
        this.chance = p_i1394_1_;
        this.bottomOffset = p_i1394_2_;
        this.topOffset = p_i1394_3_;
        this.top = p_i1394_4_;
    }
}

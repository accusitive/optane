package net.minecraft.network.login.server;

import com.mojang.authlib.GameProfile;
import java.io.IOException;
import java.util.UUID;
import net.minecraft.client.network.login.IClientLoginNetHandler;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.UUIDCodec;

public class SLoginSuccessPacket implements IPacket<IClientLoginNetHandler>
{
    private GameProfile profile;

    public SLoginSuccessPacket()
    {
    }

    public SLoginSuccessPacket(GameProfile p_i969_1_)
    {
        this.profile = p_i969_1_;
    }

    public void readPacketData(PacketBuffer buf) throws IOException
    {
        int[] aint = new int[4];

        for (int i = 0; i < aint.length; ++i)
        {
            aint[i] = buf.readInt();
        }

        UUID uuid = UUIDCodec.func_239779_a_(aint);
        String s = buf.readString(16);
        this.profile = new GameProfile(uuid, s);
    }

    public void writePacketData(PacketBuffer buf) throws IOException
    {
        for (int i : UUIDCodec.func_239777_a_(this.profile.getId()))
        {
            buf.writeInt(i);
        }

        buf.writeString(this.profile.getName());
    }

    public void processPacket(IClientLoginNetHandler handler)
    {
        handler.handleLoginSuccess(this);
    }

    public GameProfile getProfile()
    {
        return this.profile;
    }
}

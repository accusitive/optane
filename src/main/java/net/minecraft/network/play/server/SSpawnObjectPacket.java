package net.minecraft.network.play.server;

import java.io.IOException;
import java.util.UUID;
import net.minecraft.client.network.play.IClientPlayNetHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.registry.Registry;

public class SSpawnObjectPacket implements IPacket<IClientPlayNetHandler>
{
    private int entityId;
    private UUID uniqueId;
    private double x;
    private double y;
    private double z;
    private int speedX;
    private int speedY;
    private int speedZ;
    private int pitch;
    private int yaw;
    private EntityType<?> type;
    private int data;

    public SSpawnObjectPacket()
    {
    }

    public SSpawnObjectPacket(int p_i274_1_, UUID p_i274_2_, double p_i274_3_, double p_i274_5_, double p_i274_7_, float p_i274_9_, float p_i274_10_, EntityType<?> p_i274_11_, int p_i274_12_, Vector3d p_i274_13_)
    {
        this.entityId = p_i274_1_;
        this.uniqueId = p_i274_2_;
        this.x = p_i274_3_;
        this.y = p_i274_5_;
        this.z = p_i274_7_;
        this.pitch = MathHelper.floor(p_i274_9_ * 256.0F / 360.0F);
        this.yaw = MathHelper.floor(p_i274_10_ * 256.0F / 360.0F);
        this.type = p_i274_11_;
        this.data = p_i274_12_;
        this.speedX = (int)(MathHelper.clamp(p_i274_13_.x, -3.9D, 3.9D) * 8000.0D);
        this.speedY = (int)(MathHelper.clamp(p_i274_13_.y, -3.9D, 3.9D) * 8000.0D);
        this.speedZ = (int)(MathHelper.clamp(p_i274_13_.z, -3.9D, 3.9D) * 8000.0D);
    }

    public SSpawnObjectPacket(Entity p_i275_1_)
    {
        this(p_i275_1_, 0);
    }

    public SSpawnObjectPacket(Entity p_i276_1_, int p_i276_2_)
    {
        this(p_i276_1_.getEntityId(), p_i276_1_.getUniqueID(), p_i276_1_.getPosX(), p_i276_1_.getPosY(), p_i276_1_.getPosZ(), p_i276_1_.rotationPitch, p_i276_1_.rotationYaw, p_i276_1_.getType(), p_i276_2_, p_i276_1_.getMotion());
    }

    public SSpawnObjectPacket(Entity p_i277_1_, EntityType<?> p_i277_2_, int p_i277_3_, BlockPos p_i277_4_)
    {
        this(p_i277_1_.getEntityId(), p_i277_1_.getUniqueID(), (double)p_i277_4_.getX(), (double)p_i277_4_.getY(), (double)p_i277_4_.getZ(), p_i277_1_.rotationPitch, p_i277_1_.rotationYaw, p_i277_2_, p_i277_3_, p_i277_1_.getMotion());
    }

    public void readPacketData(PacketBuffer buf) throws IOException
    {
        this.entityId = buf.readVarInt();
        this.uniqueId = buf.readUniqueId();
        this.type = Registry.ENTITY_TYPE.getByValue(buf.readVarInt());
        this.x = buf.readDouble();
        this.y = buf.readDouble();
        this.z = buf.readDouble();
        this.pitch = buf.readByte();
        this.yaw = buf.readByte();
        this.data = buf.readInt();
        this.speedX = buf.readShort();
        this.speedY = buf.readShort();
        this.speedZ = buf.readShort();
    }

    public void writePacketData(PacketBuffer buf) throws IOException
    {
        buf.writeVarInt(this.entityId);
        buf.writeUniqueId(this.uniqueId);
        buf.writeVarInt(Registry.ENTITY_TYPE.getId(this.type));
        buf.writeDouble(this.x);
        buf.writeDouble(this.y);
        buf.writeDouble(this.z);
        buf.writeByte(this.pitch);
        buf.writeByte(this.yaw);
        buf.writeInt(this.data);
        buf.writeShort(this.speedX);
        buf.writeShort(this.speedY);
        buf.writeShort(this.speedZ);
    }

    public void processPacket(IClientPlayNetHandler handler)
    {
        handler.handleSpawnObject(this);
    }

    public int getEntityID()
    {
        return this.entityId;
    }

    public UUID getUniqueId()
    {
        return this.uniqueId;
    }

    public double getX()
    {
        return this.x;
    }

    public double getY()
    {
        return this.y;
    }

    public double getZ()
    {
        return this.z;
    }

    public double func_218693_g()
    {
        return (double)this.speedX / 8000.0D;
    }

    public double func_218695_h()
    {
        return (double)this.speedY / 8000.0D;
    }

    public double func_218692_i()
    {
        return (double)this.speedZ / 8000.0D;
    }

    public int getPitch()
    {
        return this.pitch;
    }

    public int getYaw()
    {
        return this.yaw;
    }

    public EntityType<?> getType()
    {
        return this.type;
    }

    public int getData()
    {
        return this.data;
    }
}

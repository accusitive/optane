package net.minecraft.network.play.server;

import java.io.IOException;
import java.util.Collection;
import net.minecraft.client.network.play.IClientPlayNetHandler;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.storage.MapData;
import net.minecraft.world.storage.MapDecoration;

public class SMapDataPacket implements IPacket<IClientPlayNetHandler>
{
    private int mapId;
    private byte mapScale;
    private boolean trackingPosition;
    private boolean field_218730_d;
    private MapDecoration[] icons;
    private int minX;
    private int minZ;
    private int columns;
    private int rows;
    private byte[] mapDataBytes;

    public SMapDataPacket()
    {
    }

    public SMapDataPacket(int p_i409_1_, byte p_i409_2_, boolean p_i409_3_, boolean p_i409_4_, Collection<MapDecoration> p_i409_5_, byte[] p_i409_6_, int p_i409_7_, int p_i409_8_, int p_i409_9_, int p_i409_10_)
    {
        this.mapId = p_i409_1_;
        this.mapScale = p_i409_2_;
        this.trackingPosition = p_i409_3_;
        this.field_218730_d = p_i409_4_;
        this.icons = p_i409_5_.toArray(new MapDecoration[p_i409_5_.size()]);
        this.minX = p_i409_7_;
        this.minZ = p_i409_8_;
        this.columns = p_i409_9_;
        this.rows = p_i409_10_;
        this.mapDataBytes = new byte[p_i409_9_ * p_i409_10_];

        for (int i = 0; i < p_i409_9_; ++i)
        {
            for (int j = 0; j < p_i409_10_; ++j)
            {
                this.mapDataBytes[i + j * p_i409_9_] = p_i409_6_[p_i409_7_ + i + (p_i409_8_ + j) * 128];
            }
        }
    }

    public void readPacketData(PacketBuffer buf) throws IOException
    {
        this.mapId = buf.readVarInt();
        this.mapScale = buf.readByte();
        this.trackingPosition = buf.readBoolean();
        this.field_218730_d = buf.readBoolean();
        this.icons = new MapDecoration[buf.readVarInt()];

        for (int i = 0; i < this.icons.length; ++i)
        {
            MapDecoration.Type mapdecoration$type = buf.readEnumValue(MapDecoration.Type.class);
            this.icons[i] = new MapDecoration(mapdecoration$type, buf.readByte(), buf.readByte(), (byte)(buf.readByte() & 15), buf.readBoolean() ? buf.readTextComponent() : null);
        }

        this.columns = buf.readUnsignedByte();

        if (this.columns > 0)
        {
            this.rows = buf.readUnsignedByte();
            this.minX = buf.readUnsignedByte();
            this.minZ = buf.readUnsignedByte();
            this.mapDataBytes = buf.readByteArray();
        }
    }

    public void writePacketData(PacketBuffer buf) throws IOException
    {
        buf.writeVarInt(this.mapId);
        buf.writeByte(this.mapScale);
        buf.writeBoolean(this.trackingPosition);
        buf.writeBoolean(this.field_218730_d);
        buf.writeVarInt(this.icons.length);

        for (MapDecoration mapdecoration : this.icons)
        {
            buf.writeEnumValue(mapdecoration.getType());
            buf.writeByte(mapdecoration.getX());
            buf.writeByte(mapdecoration.getY());
            buf.writeByte(mapdecoration.getRotation() & 15);

            if (mapdecoration.getCustomName() != null)
            {
                buf.writeBoolean(true);
                buf.writeTextComponent(mapdecoration.getCustomName());
            }
            else
            {
                buf.writeBoolean(false);
            }
        }

        buf.writeByte(this.columns);

        if (this.columns > 0)
        {
            buf.writeByte(this.rows);
            buf.writeByte(this.minX);
            buf.writeByte(this.minZ);
            buf.writeByteArray(this.mapDataBytes);
        }
    }

    public void processPacket(IClientPlayNetHandler handler)
    {
        handler.handleMaps(this);
    }

    public int getMapId()
    {
        return this.mapId;
    }

    public void setMapdataTo(MapData mapdataIn)
    {
        mapdataIn.scale = this.mapScale;
        mapdataIn.trackingPosition = this.trackingPosition;
        mapdataIn.locked = this.field_218730_d;
        mapdataIn.mapDecorations.clear();

        for (int i = 0; i < this.icons.length; ++i)
        {
            MapDecoration mapdecoration = this.icons[i];
            mapdataIn.mapDecorations.put("icon-" + i, mapdecoration);
        }

        for (int j = 0; j < this.columns; ++j)
        {
            for (int k = 0; k < this.rows; ++k)
            {
                mapdataIn.colors[this.minX + j + (this.minZ + k) * 128] = this.mapDataBytes[j + k * this.columns];
            }
        }
    }
}

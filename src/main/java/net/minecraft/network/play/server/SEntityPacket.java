package net.minecraft.network.play.server;

import java.io.IOException;
import net.minecraft.client.network.play.IClientPlayNetHandler;
import net.minecraft.entity.Entity;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;

public class SEntityPacket implements IPacket<IClientPlayNetHandler>
{
    protected int entityId;
    protected short posX;
    protected short posY;
    protected short posZ;
    protected byte yaw;
    protected byte pitch;
    protected boolean onGround;
    protected boolean rotating;
    protected boolean isMovePacket;

    public static long func_218743_a(double p_218743_0_)
    {
        return MathHelper.lfloor(p_218743_0_ * 4096.0D);
    }

    public static Vector3d func_218744_a(long p_218744_0_, long p_218744_2_, long p_218744_4_)
    {
        return (new Vector3d((double)p_218744_0_, (double)p_218744_2_, (double)p_218744_4_)).scale((double)2.4414062E-4F);
    }

    public SEntityPacket()
    {
    }

    public SEntityPacket(int worldIn)
    {
        this.entityId = worldIn;
    }

    public void readPacketData(PacketBuffer buf) throws IOException
    {
        this.entityId = buf.readVarInt();
    }

    public void writePacketData(PacketBuffer buf) throws IOException
    {
        buf.writeVarInt(this.entityId);
    }

    public void processPacket(IClientPlayNetHandler handler)
    {
        handler.handleEntityMovement(this);
    }

    public String toString()
    {
        return "Entity_" + super.toString();
    }

    public Entity getEntity(World worldIn)
    {
        return worldIn.getEntityByID(this.entityId);
    }

    public short getX()
    {
        return this.posX;
    }

    public short getY()
    {
        return this.posY;
    }

    public short getZ()
    {
        return this.posZ;
    }

    public byte getYaw()
    {
        return this.yaw;
    }

    public byte getPitch()
    {
        return this.pitch;
    }

    public boolean isRotating()
    {
        return this.rotating;
    }

    public boolean func_229745_h_()
    {
        return this.isMovePacket;
    }

    public boolean getOnGround()
    {
        return this.onGround;
    }

    public static class LookPacket extends SEntityPacket
    {
        public LookPacket()
        {
            this.rotating = true;
        }

        public LookPacket(int p_i4172_1_, byte p_i4172_2_, byte p_i4172_3_, boolean p_i4172_4_)
        {
            super(p_i4172_1_);
            this.yaw = p_i4172_2_;
            this.pitch = p_i4172_3_;
            this.rotating = true;
            this.onGround = p_i4172_4_;
        }

        public void readPacketData(PacketBuffer buf) throws IOException
        {
            super.readPacketData(buf);
            this.yaw = buf.readByte();
            this.pitch = buf.readByte();
            this.onGround = buf.readBoolean();
        }

        public void writePacketData(PacketBuffer buf) throws IOException
        {
            super.writePacketData(buf);
            buf.writeByte(this.yaw);
            buf.writeByte(this.pitch);
            buf.writeBoolean(this.onGround);
        }
    }

    public static class MovePacket extends SEntityPacket
    {
        public MovePacket()
        {
            this.rotating = true;
            this.isMovePacket = true;
        }

        public MovePacket(int p_i2825_1_, short p_i2825_2_, short p_i2825_3_, short p_i2825_4_, byte p_i2825_5_, byte p_i2825_6_, boolean p_i2825_7_)
        {
            super(p_i2825_1_);
            this.posX = p_i2825_2_;
            this.posY = p_i2825_3_;
            this.posZ = p_i2825_4_;
            this.yaw = p_i2825_5_;
            this.pitch = p_i2825_6_;
            this.onGround = p_i2825_7_;
            this.rotating = true;
            this.isMovePacket = true;
        }

        public void readPacketData(PacketBuffer buf) throws IOException
        {
            super.readPacketData(buf);
            this.posX = buf.readShort();
            this.posY = buf.readShort();
            this.posZ = buf.readShort();
            this.yaw = buf.readByte();
            this.pitch = buf.readByte();
            this.onGround = buf.readBoolean();
        }

        public void writePacketData(PacketBuffer buf) throws IOException
        {
            super.writePacketData(buf);
            buf.writeShort(this.posX);
            buf.writeShort(this.posY);
            buf.writeShort(this.posZ);
            buf.writeByte(this.yaw);
            buf.writeByte(this.pitch);
            buf.writeBoolean(this.onGround);
        }
    }

    public static class RelativeMovePacket extends SEntityPacket
    {
        public RelativeMovePacket()
        {
            this.isMovePacket = true;
        }

        public RelativeMovePacket(int p_i1447_1_, short p_i1447_2_, short p_i1447_3_, short p_i1447_4_, boolean p_i1447_5_)
        {
            super(p_i1447_1_);
            this.posX = p_i1447_2_;
            this.posY = p_i1447_3_;
            this.posZ = p_i1447_4_;
            this.onGround = p_i1447_5_;
            this.isMovePacket = true;
        }

        public void readPacketData(PacketBuffer buf) throws IOException
        {
            super.readPacketData(buf);
            this.posX = buf.readShort();
            this.posY = buf.readShort();
            this.posZ = buf.readShort();
            this.onGround = buf.readBoolean();
        }

        public void writePacketData(PacketBuffer buf) throws IOException
        {
            super.writePacketData(buf);
            buf.writeShort(this.posX);
            buf.writeShort(this.posY);
            buf.writeShort(this.posZ);
            buf.writeBoolean(this.onGround);
        }
    }
}

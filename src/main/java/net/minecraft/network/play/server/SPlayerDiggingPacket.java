package net.minecraft.network.play.server;

import java.io.IOException;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.network.play.IClientPlayNetHandler;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.play.client.CPlayerDiggingPacket;
import net.minecraft.util.math.BlockPos;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SPlayerDiggingPacket implements IPacket<IClientPlayNetHandler>
{
    private static final Logger LOGGER = LogManager.getLogger();
    private BlockPos pos;
    private BlockState state;
    CPlayerDiggingPacket.Action action;
    private boolean successful;

    public SPlayerDiggingPacket()
    {
    }

    public SPlayerDiggingPacket(BlockPos p_i4174_1_, BlockState p_i4174_2_, CPlayerDiggingPacket.Action p_i4174_3_, boolean p_i4174_4_, String p_i4174_5_)
    {
        this.pos = p_i4174_1_.toImmutable();
        this.state = p_i4174_2_;
        this.action = p_i4174_3_;
        this.successful = p_i4174_4_;
    }

    public void readPacketData(PacketBuffer buf) throws IOException
    {
        this.pos = buf.readBlockPos();
        this.state = Block.BLOCK_STATE_IDS.getByValue(buf.readVarInt());
        this.action = buf.readEnumValue(CPlayerDiggingPacket.Action.class);
        this.successful = buf.readBoolean();
    }

    public void writePacketData(PacketBuffer buf) throws IOException
    {
        buf.writeBlockPos(this.pos);
        buf.writeVarInt(Block.getStateId(this.state));
        buf.writeEnumValue(this.action);
        buf.writeBoolean(this.successful);
    }

    public void processPacket(IClientPlayNetHandler handler)
    {
        handler.handleAcknowledgePlayerDigging(this);
    }

    public BlockState getBlockState()
    {
        return this.state;
    }

    public BlockPos getPosition()
    {
        return this.pos;
    }

    public boolean wasSuccessful()
    {
        return this.successful;
    }

    public CPlayerDiggingPacket.Action getAction()
    {
        return this.action;
    }
}

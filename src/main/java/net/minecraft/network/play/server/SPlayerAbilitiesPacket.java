package net.minecraft.network.play.server;

import java.io.IOException;
import net.minecraft.client.network.play.IClientPlayNetHandler;
import net.minecraft.entity.player.PlayerAbilities;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;

public class SPlayerAbilitiesPacket implements IPacket<IClientPlayNetHandler>
{
    private boolean invulnerable;
    private boolean flying;
    private boolean allowFlying;
    private boolean creativeMode;
    private float flySpeed;
    private float walkSpeed;

    public SPlayerAbilitiesPacket()
    {
    }

    public SPlayerAbilitiesPacket(PlayerAbilities p_i979_1_)
    {
        this.invulnerable = p_i979_1_.disableDamage;
        this.flying = p_i979_1_.isFlying;
        this.allowFlying = p_i979_1_.allowFlying;
        this.creativeMode = p_i979_1_.isCreativeMode;
        this.flySpeed = p_i979_1_.getFlySpeed();
        this.walkSpeed = p_i979_1_.getWalkSpeed();
    }

    public void readPacketData(PacketBuffer buf) throws IOException
    {
        byte b0 = buf.readByte();
        this.invulnerable = (b0 & 1) != 0;
        this.flying = (b0 & 2) != 0;
        this.allowFlying = (b0 & 4) != 0;
        this.creativeMode = (b0 & 8) != 0;
        this.flySpeed = buf.readFloat();
        this.walkSpeed = buf.readFloat();
    }

    public void writePacketData(PacketBuffer buf) throws IOException
    {
        byte b0 = 0;

        if (this.invulnerable)
        {
            b0 = (byte)(b0 | 1);
        }

        if (this.flying)
        {
            b0 = (byte)(b0 | 2);
        }

        if (this.allowFlying)
        {
            b0 = (byte)(b0 | 4);
        }

        if (this.creativeMode)
        {
            b0 = (byte)(b0 | 8);
        }

        buf.writeByte(b0);
        buf.writeFloat(this.flySpeed);
        buf.writeFloat(this.walkSpeed);
    }

    public void processPacket(IClientPlayNetHandler handler)
    {
        handler.handlePlayerAbilities(this);
    }

    public boolean isInvulnerable()
    {
        return this.invulnerable;
    }

    public boolean isFlying()
    {
        return this.flying;
    }

    public boolean isAllowFlying()
    {
        return this.allowFlying;
    }

    public boolean isCreativeMode()
    {
        return this.creativeMode;
    }

    public float getFlySpeed()
    {
        return this.flySpeed;
    }

    public float getWalkSpeed()
    {
        return this.walkSpeed;
    }
}

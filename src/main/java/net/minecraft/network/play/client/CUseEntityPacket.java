package net.minecraft.network.play.client;

import java.io.IOException;
import javax.annotation.Nullable;
import net.minecraft.entity.Entity;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.play.IServerPlayNetHandler;
import net.minecraft.util.Hand;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;

public class CUseEntityPacket implements IPacket<IServerPlayNetHandler>
{
    private int entityId;
    private CUseEntityPacket.Action action;
    private Vector3d hitVec;
    private Hand hand;
    private boolean field_241791_e_;

    public CUseEntityPacket()
    {
    }

    public CUseEntityPacket(Entity p_i2938_1_, boolean p_i2938_2_)
    {
        this.entityId = p_i2938_1_.getEntityId();
        this.action = CUseEntityPacket.Action.ATTACK;
        this.field_241791_e_ = p_i2938_2_;
    }

    public CUseEntityPacket(Entity p_i2939_1_, Hand p_i2939_2_, boolean p_i2939_3_)
    {
        this.entityId = p_i2939_1_.getEntityId();
        this.action = CUseEntityPacket.Action.INTERACT;
        this.hand = p_i2939_2_;
        this.field_241791_e_ = p_i2939_3_;
    }

    public CUseEntityPacket(Entity p_i2940_1_, Hand p_i2940_2_, Vector3d p_i2940_3_, boolean p_i2940_4_)
    {
        this.entityId = p_i2940_1_.getEntityId();
        this.action = CUseEntityPacket.Action.INTERACT_AT;
        this.hand = p_i2940_2_;
        this.hitVec = p_i2940_3_;
        this.field_241791_e_ = p_i2940_4_;
    }

    public void readPacketData(PacketBuffer buf) throws IOException
    {
        this.entityId = buf.readVarInt();
        this.action = buf.readEnumValue(CUseEntityPacket.Action.class);

        if (this.action == CUseEntityPacket.Action.INTERACT_AT)
        {
            this.hitVec = new Vector3d((double)buf.readFloat(), (double)buf.readFloat(), (double)buf.readFloat());
        }

        if (this.action == CUseEntityPacket.Action.INTERACT || this.action == CUseEntityPacket.Action.INTERACT_AT)
        {
            this.hand = buf.readEnumValue(Hand.class);
        }

        this.field_241791_e_ = buf.readBoolean();
    }

    public void writePacketData(PacketBuffer buf) throws IOException
    {
        buf.writeVarInt(this.entityId);
        buf.writeEnumValue(this.action);

        if (this.action == CUseEntityPacket.Action.INTERACT_AT)
        {
            buf.writeFloat((float)this.hitVec.x);
            buf.writeFloat((float)this.hitVec.y);
            buf.writeFloat((float)this.hitVec.z);
        }

        if (this.action == CUseEntityPacket.Action.INTERACT || this.action == CUseEntityPacket.Action.INTERACT_AT)
        {
            buf.writeEnumValue(this.hand);
        }

        buf.writeBoolean(this.field_241791_e_);
    }

    public void processPacket(IServerPlayNetHandler handler)
    {
        handler.processUseEntity(this);
    }

    @Nullable
    public Entity getEntityFromWorld(World worldIn)
    {
        return worldIn.getEntityByID(this.entityId);
    }

    public CUseEntityPacket.Action getAction()
    {
        return this.action;
    }

    public Hand getHand()
    {
        return this.hand;
    }

    public Vector3d getHitVec()
    {
        return this.hitVec;
    }

    public boolean func_241792_e_()
    {
        return this.field_241791_e_;
    }

    public static enum Action
    {
        INTERACT,
        ATTACK,
        INTERACT_AT;
    }
}

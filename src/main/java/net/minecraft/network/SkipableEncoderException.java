package net.minecraft.network;

import io.netty.handler.codec.EncoderException;

public class SkipableEncoderException extends EncoderException
{
    public SkipableEncoderException(Throwable p_i1915_1_)
    {
        super(p_i1915_1_);
    }
}

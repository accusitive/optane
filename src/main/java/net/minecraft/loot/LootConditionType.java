package net.minecraft.loot;

import net.minecraft.loot.conditions.ILootCondition;

public class LootConditionType extends LootType<ILootCondition>
{
    public LootConditionType(ILootSerializer <? extends ILootCondition > p_i4229_1_)
    {
        super(p_i4229_1_);
    }
}

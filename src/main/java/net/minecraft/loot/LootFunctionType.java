package net.minecraft.loot;

import net.minecraft.loot.functions.ILootFunction;

public class LootFunctionType extends LootType<ILootFunction>
{
    public LootFunctionType(ILootSerializer <? extends ILootFunction > p_i3798_1_)
    {
        super(p_i3798_1_);
    }
}

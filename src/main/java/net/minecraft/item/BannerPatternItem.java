package net.minecraft.item;

import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.tileentity.BannerPattern;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class BannerPatternItem extends Item
{
    private final BannerPattern pattern;

    public BannerPatternItem(BannerPattern p_i3485_1_, Item.Properties p_i3485_2_)
    {
        super(p_i3485_2_);
        this.pattern = p_i3485_1_;
    }

    public BannerPattern getBannerPattern()
    {
        return this.pattern;
    }

    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn)
    {
        tooltip.add(this.func_219981_d_().func_240699_a_(TextFormatting.GRAY));
    }

    public IFormattableTextComponent func_219981_d_()
    {
        return new TranslationTextComponent(this.getTranslationKey() + ".desc");
    }
}

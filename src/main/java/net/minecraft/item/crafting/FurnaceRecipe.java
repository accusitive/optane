package net.minecraft.item.crafting;

import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class FurnaceRecipe extends AbstractCookingRecipe
{
    public FurnaceRecipe(ResourceLocation p_i4358_1_, String p_i4358_2_, Ingredient p_i4358_3_, ItemStack p_i4358_4_, float p_i4358_5_, int p_i4358_6_)
    {
        super(IRecipeType.SMELTING, p_i4358_1_, p_i4358_2_, p_i4358_3_, p_i4358_4_, p_i4358_5_, p_i4358_6_);
    }

    public ItemStack getIcon()
    {
        return new ItemStack(Blocks.FURNACE);
    }

    public IRecipeSerializer<?> getSerializer()
    {
        return IRecipeSerializer.SMELTING;
    }
}

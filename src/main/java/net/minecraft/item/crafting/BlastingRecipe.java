package net.minecraft.item.crafting;

import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class BlastingRecipe extends AbstractCookingRecipe
{
    public BlastingRecipe(ResourceLocation p_i3218_1_, String p_i3218_2_, Ingredient p_i3218_3_, ItemStack p_i3218_4_, float p_i3218_5_, int p_i3218_6_)
    {
        super(IRecipeType.BLASTING, p_i3218_1_, p_i3218_2_, p_i3218_3_, p_i3218_4_, p_i3218_5_, p_i3218_6_);
    }

    public ItemStack getIcon()
    {
        return new ItemStack(Blocks.BLAST_FURNACE);
    }

    public IRecipeSerializer<?> getSerializer()
    {
        return IRecipeSerializer.BLASTING;
    }
}

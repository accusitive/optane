package net.minecraft.item;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;

public class TallBlockItem extends BlockItem
{
    public TallBlockItem(Block p_i3930_1_, Item.Properties p_i3930_2_)
    {
        super(p_i3930_1_, p_i3930_2_);
    }

    protected boolean placeBlock(BlockItemUseContext context, BlockState state)
    {
        context.getWorld().setBlockState(context.getPos().up(), Blocks.AIR.getDefaultState(), 27);
        return super.placeBlock(context, state);
    }
}

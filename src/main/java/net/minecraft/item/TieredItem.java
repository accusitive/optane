package net.minecraft.item;

public class TieredItem extends Item
{
    private final IItemTier tier;

    public TieredItem(IItemTier p_i1747_1_, Item.Properties p_i1747_2_)
    {
        super(p_i1747_2_.defaultMaxDamage(p_i1747_1_.getMaxUses()));
        this.tier = p_i1747_1_;
    }

    public IItemTier getTier()
    {
        return this.tier;
    }

    public int getItemEnchantability()
    {
        return this.tier.getEnchantability();
    }

    public boolean getIsRepairable(ItemStack toRepair, ItemStack repair)
    {
        return this.tier.getRepairMaterial().test(repair) || super.getIsRepairable(toRepair, repair);
    }
}

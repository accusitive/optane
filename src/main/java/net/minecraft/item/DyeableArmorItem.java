package net.minecraft.item;

import net.minecraft.inventory.EquipmentSlotType;

public class DyeableArmorItem extends ArmorItem implements IDyeableArmorItem
{
    public DyeableArmorItem(IArmorMaterial entitylivingIn, EquipmentSlotType p_i1647_2_, Item.Properties p_i1647_3_)
    {
        super(entitylivingIn, p_i1647_2_, p_i1647_3_);
    }
}

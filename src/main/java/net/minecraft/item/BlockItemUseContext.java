package net.minecraft.item;

import javax.annotation.Nullable;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;

public class BlockItemUseContext extends ItemUseContext
{
    private final BlockPos offsetPos;
    protected boolean replaceClicked = true;

    public BlockItemUseContext(PlayerEntity p_i3432_1_, Hand p_i3432_2_, ItemStack p_i3432_3_, BlockRayTraceResult p_i3432_4_)
    {
        this(p_i3432_1_.world, p_i3432_1_, p_i3432_2_, p_i3432_3_, p_i3432_4_);
    }

    public BlockItemUseContext(ItemUseContext p_i3433_1_)
    {
        this(p_i3433_1_.getWorld(), p_i3433_1_.getPlayer(), p_i3433_1_.getHand(), p_i3433_1_.getItem(), p_i3433_1_.rayTraceResult);
    }

    protected BlockItemUseContext(World p_i3434_1_, @Nullable PlayerEntity p_i3434_2_, Hand p_i3434_3_, ItemStack p_i3434_4_, BlockRayTraceResult p_i3434_5_)
    {
        super(p_i3434_1_, p_i3434_2_, p_i3434_3_, p_i3434_4_, p_i3434_5_);
        this.offsetPos = p_i3434_5_.getPos().offset(p_i3434_5_.getFace());
        this.replaceClicked = p_i3434_1_.getBlockState(p_i3434_5_.getPos()).isReplaceable(this);
    }

    public static BlockItemUseContext func_221536_a(BlockItemUseContext context, BlockPos pos, Direction directionIn)
    {
        return new BlockItemUseContext(context.getWorld(), context.getPlayer(), context.getHand(), context.getItem(), new BlockRayTraceResult(new Vector3d((double)pos.getX() + 0.5D + (double)directionIn.getXOffset() * 0.5D, (double)pos.getY() + 0.5D + (double)directionIn.getYOffset() * 0.5D, (double)pos.getZ() + 0.5D + (double)directionIn.getZOffset() * 0.5D), directionIn, pos, false));
    }

    public BlockPos getPos()
    {
        return this.replaceClicked ? super.getPos() : this.offsetPos;
    }

    public boolean canPlace()
    {
        return this.replaceClicked || this.getWorld().getBlockState(this.getPos()).isReplaceable(this);
    }

    public boolean replacingClickedOnBlock()
    {
        return this.replaceClicked;
    }

    public Direction getNearestLookingDirection()
    {
        return Direction.getFacingDirections(this.player)[0];
    }

    public Direction[] getNearestLookingDirections()
    {
        Direction[] adirection = Direction.getFacingDirections(this.player);

        if (this.replaceClicked)
        {
            return adirection;
        }
        else
        {
            Direction direction = this.getFace();
            int i;

            for (i = 0; i < adirection.length && adirection[i] != direction.getOpposite(); ++i)
            {
            }

            if (i > 0)
            {
                System.arraycopy(adirection, 0, adirection, 1, i);
                adirection[0] = direction.getOpposite();
            }

            return adirection;
        }
    }
}

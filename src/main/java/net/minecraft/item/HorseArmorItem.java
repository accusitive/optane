package net.minecraft.item;

import net.minecraft.util.ResourceLocation;

public class HorseArmorItem extends Item
{
    private final int armorValue;
    private final String field_219979_b;

    public HorseArmorItem(int p_i2879_1_, String p_i2879_2_, Item.Properties p_i2879_3_)
    {
        super(p_i2879_3_);
        this.armorValue = p_i2879_1_;
        this.field_219979_b = "textures/entity/horse/armor/horse_armor_" + p_i2879_2_ + ".png";
    }

    public ResourceLocation getArmorTexture()
    {
        return new ResourceLocation(this.field_219979_b);
    }

    public int getArmorValue()
    {
        return this.armorValue;
    }
}

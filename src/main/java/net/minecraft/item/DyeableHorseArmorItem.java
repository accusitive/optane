package net.minecraft.item;

public class DyeableHorseArmorItem extends HorseArmorItem implements IDyeableArmorItem
{
    public DyeableHorseArmorItem(int p_i3423_1_, String p_i3423_2_, Item.Properties p_i3423_3_)
    {
        super(p_i3423_1_, p_i3423_2_, p_i3423_3_);
    }
}

package net.minecraft.item;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.ImmutableMultimap.Builder;
import java.util.Set;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.IVanishable;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ToolItem extends TieredItem implements IVanishable
{
    private final Set<Block> effectiveBlocks;
    protected final float efficiency;
    private final float attackDamage;
    private final Multimap<Attribute, AttributeModifier> field_234674_d_;

    protected ToolItem(float p_i4446_1_, float p_i4446_2_, IItemTier p_i4446_3_, Set<Block> p_i4446_4_, Item.Properties p_i4446_5_)
    {
        super(p_i4446_3_, p_i4446_5_);
        this.effectiveBlocks = p_i4446_4_;
        this.efficiency = p_i4446_3_.getEfficiency();
        this.attackDamage = p_i4446_1_ + p_i4446_3_.getAttackDamage();
        Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
        builder.put(Attributes.field_233823_f_, new AttributeModifier(ATTACK_DAMAGE_MODIFIER, "Tool modifier", (double)this.attackDamage, AttributeModifier.Operation.ADDITION));
        builder.put(Attributes.field_233825_h_, new AttributeModifier(ATTACK_SPEED_MODIFIER, "Tool modifier", (double)p_i4446_2_, AttributeModifier.Operation.ADDITION));
        this.field_234674_d_ = builder.build();
    }

    public float getDestroySpeed(ItemStack stack, BlockState state)
    {
        return this.effectiveBlocks.contains(state.getBlock()) ? this.efficiency : 1.0F;
    }

    public boolean hitEntity(ItemStack stack, LivingEntity target, LivingEntity attacker)
    {
        stack.damageItem(2, attacker, (p_220039_0_) ->
        {
            p_220039_0_.sendBreakAnimation(EquipmentSlotType.MAINHAND);
        });
        return true;
    }

    public boolean onBlockDestroyed(ItemStack stack, World worldIn, BlockState state, BlockPos pos, LivingEntity entityLiving)
    {
        if (!worldIn.isRemote && state.getBlockHardness(worldIn, pos) != 0.0F)
        {
            stack.damageItem(1, entityLiving, (p_220038_0_) ->
            {
                p_220038_0_.sendBreakAnimation(EquipmentSlotType.MAINHAND);
            });
        }

        return true;
    }

    public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlotType equipmentSlot)
    {
        return equipmentSlot == EquipmentSlotType.MAINHAND ? this.field_234674_d_ : super.getAttributeModifiers(equipmentSlot);
    }

    public float func_234675_d_()
    {
        return this.attackDamage;
    }
}

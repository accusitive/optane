package net.minecraft.item;

import net.minecraft.block.Block;

public class BlockNamedItem extends BlockItem
{
    public BlockNamedItem(Block p_i1255_1_, Item.Properties p_i1255_2_)
    {
        super(p_i1255_1_, p_i1255_2_);
    }

    public String getTranslationKey()
    {
        return this.getDefaultTranslationKey();
    }
}

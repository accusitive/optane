package net.minecraft.particles;

import com.mojang.serialization.Codec;

public abstract class ParticleType<T extends IParticleData>
{
    private final boolean alwaysShow;
    private final IParticleData.IDeserializer<T> deserializer;

    protected ParticleType(boolean p_i1452_1_, IParticleData.IDeserializer<T> p_i1452_2_)
    {
        this.alwaysShow = p_i1452_1_;
        this.deserializer = p_i1452_2_;
    }

    public boolean getAlwaysShow()
    {
        return this.alwaysShow;
    }

    public IParticleData.IDeserializer<T> getDeserializer()
    {
        return this.deserializer;
    }

    public abstract Codec<T> func_230522_e_();
}

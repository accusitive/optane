package net.minecraft.util;

import net.minecraft.nbt.CompoundNBT;

public class WeightedSpawnerEntity extends WeightedRandom.Item
{
    private final CompoundNBT nbt;

    public WeightedSpawnerEntity()
    {
        super(1);
        this.nbt = new CompoundNBT();
        this.nbt.putString("id", "minecraft:pig");
    }

    public WeightedSpawnerEntity(CompoundNBT p_i3836_1_)
    {
        this(p_i3836_1_.contains("Weight", 99) ? p_i3836_1_.getInt("Weight") : 1, p_i3836_1_.getCompound("Entity"));
    }

    public WeightedSpawnerEntity(int p_i3837_1_, CompoundNBT p_i3837_2_)
    {
        super(p_i3837_1_);
        this.nbt = p_i3837_2_;
        ResourceLocation resourcelocation = ResourceLocation.tryCreate(p_i3837_2_.getString("id"));

        if (resourcelocation != null)
        {
            p_i3837_2_.putString("id", resourcelocation.toString());
        }
        else
        {
            p_i3837_2_.putString("id", "minecraft:pig");
        }
    }

    public CompoundNBT toCompoundTag()
    {
        CompoundNBT compoundnbt = new CompoundNBT();
        compoundnbt.put("Entity", this.nbt);
        compoundnbt.putInt("Weight", this.itemWeight);
        return compoundnbt;
    }

    public CompoundNBT getNbt()
    {
        return this.nbt;
    }
}

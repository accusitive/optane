package net.minecraft.util.math.shapes;

import java.util.function.Predicate;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.fluid.FlowingFluid;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;

public class EntitySelectionContext implements ISelectionContext
{
    protected static final ISelectionContext DUMMY = new EntitySelectionContext(false, -Double.MAX_VALUE, Items.AIR, (p_237495_0_) ->
    {
        return false;
    })
    {
        public boolean func_216378_a(VoxelShape shape, BlockPos pos, boolean p_216378_3_)
        {
            return p_216378_3_;
        }
    };
    private final boolean sneaking;
    private final double posY;
    private final Item item;
    private final Predicate<Fluid> field_237493_e_;

    protected EntitySelectionContext(boolean data, double p_i1389_2_, Item p_i1389_4_, Predicate<Fluid> p_i1389_5_)
    {
        this.sneaking = data;
        this.posY = p_i1389_2_;
        this.item = p_i1389_4_;
        this.field_237493_e_ = p_i1389_5_;
    }

    @Deprecated
    protected EntitySelectionContext(Entity p_i1390_1_)
    {
        this(p_i1390_1_.isDescending(), p_i1390_1_.getPosY(), p_i1390_1_ instanceof LivingEntity ? ((LivingEntity)p_i1390_1_).getHeldItemMainhand().getItem() : Items.AIR, p_i1390_1_ instanceof LivingEntity ? ((LivingEntity)p_i1390_1_)::func_230285_a_ : (p_237494_0_) ->
        {
            return false;
        });
    }

    public boolean hasItem(Item itemIn)
    {
        return this.item == itemIn;
    }

    public boolean func_230426_a_(FluidState p_230426_1_, FlowingFluid p_230426_2_)
    {
        return this.field_237493_e_.test(p_230426_2_) && !p_230426_1_.getFluid().isEquivalentTo(p_230426_2_);
    }

    public boolean getPosY()
    {
        return this.sneaking;
    }

    public boolean func_216378_a(VoxelShape shape, BlockPos pos, boolean p_216378_3_)
    {
        return this.posY > (double)pos.getY() + shape.getEnd(Direction.Axis.Y) - (double)1.0E-5F;
    }
}

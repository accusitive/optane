package net.minecraft.util.math;

public class Tuple3d
{
    public double x;
    public double y;
    public double z;

    public Tuple3d(double p_i1438_1_, double p_i1438_3_, double p_i1438_5_)
    {
        this.x = p_i1438_1_;
        this.y = p_i1438_3_;
        this.z = p_i1438_5_;
    }
}

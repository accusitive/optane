package net.minecraft.util.math;

import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Vector3d;

public class BlockRayTraceResult extends RayTraceResult
{
    private final Direction face;
    private final BlockPos pos;
    private final boolean isMiss;
    private final boolean inside;

    public static BlockRayTraceResult createMiss(Vector3d hitVec, Direction faceIn, BlockPos posIn)
    {
        return new BlockRayTraceResult(true, hitVec, faceIn, posIn, false);
    }

    public BlockRayTraceResult(Vector3d p_i856_1_, Direction p_i856_2_, BlockPos p_i856_3_, boolean p_i856_4_)
    {
        this(false, p_i856_1_, p_i856_2_, p_i856_3_, p_i856_4_);
    }

    private BlockRayTraceResult(boolean p_i857_1_, Vector3d p_i857_2_, Direction p_i857_3_, BlockPos p_i857_4_, boolean p_i857_5_)
    {
        super(p_i857_2_);
        this.isMiss = p_i857_1_;
        this.face = p_i857_3_;
        this.pos = p_i857_4_;
        this.inside = p_i857_5_;
    }

    public BlockRayTraceResult withFace(Direction newFace)
    {
        return new BlockRayTraceResult(this.isMiss, this.hitResult, newFace, this.pos, this.inside);
    }

    public BlockRayTraceResult func_237485_a_(BlockPos p_237485_1_)
    {
        return new BlockRayTraceResult(this.isMiss, this.hitResult, this.face, p_237485_1_, this.inside);
    }

    public BlockPos getPos()
    {
        return this.pos;
    }

    public Direction getFace()
    {
        return this.face;
    }

    public RayTraceResult.Type getType()
    {
        return this.isMiss ? RayTraceResult.Type.MISS : RayTraceResult.Type.BLOCK;
    }

    public boolean isInside()
    {
        return this.inside;
    }
}

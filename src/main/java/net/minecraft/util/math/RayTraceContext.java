package net.minecraft.util.math;

import java.util.function.Predicate;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.fluid.FluidState;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.IBlockReader;

public class RayTraceContext
{
    private final Vector3d startVec;
    private final Vector3d endVec;
    private final RayTraceContext.BlockMode blockMode;
    private final RayTraceContext.FluidMode fluidMode;
    private final ISelectionContext context;

    public RayTraceContext(Vector3d p_i2628_1_, Vector3d p_i2628_2_, RayTraceContext.BlockMode p_i2628_3_, RayTraceContext.FluidMode p_i2628_4_, Entity p_i2628_5_)
    {
        this.startVec = p_i2628_1_;
        this.endVec = p_i2628_2_;
        this.blockMode = p_i2628_3_;
        this.fluidMode = p_i2628_4_;
        this.context = ISelectionContext.forEntity(p_i2628_5_);
    }

    public Vector3d getEndVec()
    {
        return this.endVec;
    }

    public Vector3d getStartVec()
    {
        return this.startVec;
    }

    public VoxelShape getBlockShape(BlockState blockStateIn, IBlockReader worldIn, BlockPos pos)
    {
        return this.blockMode.get(blockStateIn, worldIn, pos, this.context);
    }

    public VoxelShape getFluidShape(FluidState stateIn, IBlockReader worldIn, BlockPos pos)
    {
        return this.fluidMode.test(stateIn) ? stateIn.getShape(worldIn, pos) : VoxelShapes.empty();
    }

    public static enum BlockMode implements RayTraceContext.IVoxelProvider
    {
        COLLIDER(AbstractBlock.AbstractBlockState::getCollisionShape),
        OUTLINE(AbstractBlock.AbstractBlockState::getShape),
        VISUAL(AbstractBlock.AbstractBlockState::getRaytraceShape);

        private final RayTraceContext.IVoxelProvider provider;

        private BlockMode(RayTraceContext.IVoxelProvider p_i3774_3_)
        {
            this.provider = p_i3774_3_;
        }

        public VoxelShape get(BlockState p_get_1_, IBlockReader p_get_2_, BlockPos p_get_3_, ISelectionContext p_get_4_)
        {
            return this.provider.get(p_get_1_, p_get_2_, p_get_3_, p_get_4_);
        }
    }

    public static enum FluidMode
    {
        NONE((p_222247_0_) -> {
            return false;
        }),
        SOURCE_ONLY(FluidState::isSource),
        ANY((p_222246_0_) -> {
            return !p_222246_0_.isEmpty();
        });

        private final Predicate<FluidState> fluidTest;

        private FluidMode(Predicate<FluidState> p_i410_3_)
        {
            this.fluidTest = p_i410_3_;
        }

        public boolean test(FluidState state)
        {
            return this.fluidTest.test(state);
        }
    }

    public interface IVoxelProvider
    {
        VoxelShape get(BlockState p_get_1_, IBlockReader p_get_2_, BlockPos p_get_3_, ISelectionContext p_get_4_);
    }
}

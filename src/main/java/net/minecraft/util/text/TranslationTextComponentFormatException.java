package net.minecraft.util.text;

public class TranslationTextComponentFormatException extends IllegalArgumentException
{
    public TranslationTextComponentFormatException(TranslationTextComponent p_i500_1_, String p_i500_2_)
    {
        super(String.format("Error parsing: %s: %s", p_i500_1_, p_i500_2_));
    }

    public TranslationTextComponentFormatException(TranslationTextComponent p_i501_1_, int p_i501_2_)
    {
        super(String.format("Invalid index %d requested for %s", p_i501_2_, p_i501_1_));
    }

    public TranslationTextComponentFormatException(TranslationTextComponent p_i502_1_, Throwable p_i502_2_)
    {
        super(String.format("Error while parsing: %s", p_i502_1_), p_i502_2_);
    }
}

package net.minecraft.util;

public class ResourceLocationException extends RuntimeException
{
    public ResourceLocationException(String p_i1025_1_)
    {
        super(p_i1025_1_);
    }

    public ResourceLocationException(String p_i1026_1_, Throwable p_i1026_2_)
    {
        super(p_i1026_1_, p_i1026_2_);
    }
}

package net.minecraft.util;

import com.mojang.serialization.Codec;

public class SoundEvent
{
    public static final Codec<SoundEvent> field_232678_a_ = ResourceLocation.field_240908_a_.xmap(SoundEvent::new, (p_232679_0_) ->
    {
        return p_232679_0_.name;
    });
    private final ResourceLocation name;

    public SoundEvent(ResourceLocation p_i977_1_)
    {
        this.name = p_i977_1_;
    }

    public ResourceLocation getName()
    {
        return this.name;
    }
}

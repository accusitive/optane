package net.minecraft.client.tutorial;

public class CompletedTutorialStep implements ITutorialStep
{
    private final Tutorial tutorial;

    public CompletedTutorialStep(Tutorial p_i2627_1_)
    {
        this.tutorial = p_i2627_1_;
    }
}

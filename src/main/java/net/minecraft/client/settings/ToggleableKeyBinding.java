package net.minecraft.client.settings;

import java.util.function.BooleanSupplier;
import net.minecraft.client.util.InputMappings;

public class ToggleableKeyBinding extends KeyBinding
{
    private final BooleanSupplier getterToggle;

    public ToggleableKeyBinding(String p_i3363_1_, int p_i3363_2_, String p_i3363_3_, BooleanSupplier p_i3363_4_)
    {
        super(p_i3363_1_, InputMappings.Type.KEYSYM, p_i3363_2_, p_i3363_3_);
        this.getterToggle = p_i3363_4_;
    }

    public void setPressed(boolean valueIn)
    {
        if (this.getterToggle.getAsBoolean())
        {
            if (valueIn)
            {
                super.setPressed(!this.isKeyDown());
            }
        }
        else
        {
            super.setPressed(valueIn);
        }
    }
}

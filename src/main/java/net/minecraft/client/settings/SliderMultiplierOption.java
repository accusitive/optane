package net.minecraft.client.settings;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import net.minecraft.client.GameSettings;
import net.minecraft.util.text.ITextComponent;

public class SliderMultiplierOption extends SliderPercentageOption
{
    public SliderMultiplierOption(String p_i1865_1_, double p_i1865_2_, double p_i1865_4_, float p_i1865_6_, Function<GameSettings, Double> p_i1865_7_, BiConsumer<GameSettings, Double> p_i1865_8_, BiFunction<GameSettings, SliderPercentageOption, ITextComponent> p_i1865_9_)
    {
        super(p_i1865_1_, p_i1865_2_, p_i1865_4_, p_i1865_6_, p_i1865_7_, p_i1865_8_, p_i1865_9_);
    }

    public double normalizeValue(double value)
    {
        return Math.log(value / this.minValue) / Math.log(this.maxValue / this.minValue);
    }

    public double denormalizeValue(double value)
    {
        return this.minValue * Math.pow(Math.E, Math.log(this.maxValue / this.minValue) * value);
    }
}

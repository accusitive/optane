package net.minecraft.client.settings;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import net.minecraft.client.AbstractOption;
import net.minecraft.client.GameSettings;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.OptionButton;
import net.minecraft.util.text.ITextComponent;

public class IteratableOption extends AbstractOption
{
    protected BiConsumer<GameSettings, Integer> setter;
    protected BiFunction<GameSettings, IteratableOption, ITextComponent> getter;

    public IteratableOption(String p_i151_1_, BiConsumer<GameSettings, Integer> p_i151_2_, BiFunction<GameSettings, IteratableOption, ITextComponent> p_i151_3_)
    {
        super(p_i151_1_);
        this.setter = p_i151_2_;
        this.getter = p_i151_3_;
    }

    public void setValueIndex(GameSettings options, int valueIn)
    {
        this.setter.accept(options, valueIn);
        options.saveOptions();
    }

    public Widget createWidget(GameSettings options, int xIn, int yIn, int widthIn)
    {
        return new OptionButton(xIn, yIn, widthIn, 20, this, this.func_238157_c_(options), (p_lambda$createWidget$0_2_) ->
        {
            this.setValueIndex(options, 1);
            p_lambda$createWidget$0_2_.setMessage(this.func_238157_c_(options));
        });
    }

    public ITextComponent func_238157_c_(GameSettings p_238157_1_)
    {
        return this.getter.apply(p_238157_1_, this);
    }
}

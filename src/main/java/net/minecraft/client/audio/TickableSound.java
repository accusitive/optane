package net.minecraft.client.audio;

import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;

public abstract class TickableSound extends LocatableSound implements ITickableSound
{
    private boolean donePlaying;

    protected TickableSound(SoundEvent p_i3513_1_, SoundCategory p_i3513_2_)
    {
        super(p_i3513_1_, p_i3513_2_);
    }

    public boolean isDonePlaying()
    {
        return this.donePlaying;
    }

    protected final void func_239509_o_()
    {
        this.donePlaying = true;
        this.repeat = false;
    }
}

package net.minecraft.client.audio;

import java.io.BufferedInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import javax.sound.sampled.AudioFormat;

public class OggAudioStreamWrapper implements IAudioStream
{
    private final OggAudioStreamWrapper.IFactory field_239536_a_;
    private IAudioStream field_239537_b_;
    private final BufferedInputStream field_239538_c_;

    public OggAudioStreamWrapper(OggAudioStreamWrapper.IFactory p_i372_1_, InputStream p_i372_2_) throws IOException
    {
        this.field_239536_a_ = p_i372_1_;
        this.field_239538_c_ = new BufferedInputStream(p_i372_2_);
        this.field_239538_c_.mark(Integer.MAX_VALUE);
        this.field_239537_b_ = p_i372_1_.create(new OggAudioStreamWrapper.Stream(this.field_239538_c_));
    }

    public AudioFormat getAudioFormat()
    {
        return this.field_239537_b_.getAudioFormat();
    }

    public ByteBuffer func_216455_a(int p_216455_1_) throws IOException
    {
        ByteBuffer bytebuffer = this.field_239537_b_.func_216455_a(p_216455_1_);

        if (!bytebuffer.hasRemaining())
        {
            this.field_239537_b_.close();
            this.field_239538_c_.reset();
            this.field_239537_b_ = this.field_239536_a_.create(new OggAudioStreamWrapper.Stream(this.field_239538_c_));
            bytebuffer = this.field_239537_b_.func_216455_a(p_216455_1_);
        }

        return bytebuffer;
    }

    public void close() throws IOException
    {
        this.field_239537_b_.close();
        this.field_239538_c_.close();
    }

    @FunctionalInterface
    public interface IFactory
    {
        IAudioStream create(InputStream p_create_1_) throws IOException;
    }

    static class Stream extends FilterInputStream
    {
        private Stream(InputStream mob)
        {
            super(mob);
        }

        public void close()
        {
        }
    }
}

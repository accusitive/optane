package net.minecraft.client.audio;

import net.minecraft.entity.passive.BeeEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;

public class BeeFlightSound extends BeeSound
{
    public BeeFlightSound(BeeEntity p_i3166_1_)
    {
        super(p_i3166_1_, SoundEvents.ENTITY_BEE_LOOP, SoundCategory.NEUTRAL);
    }

    protected TickableSound getNextSound()
    {
        return new BeeAngrySound(this.beeInstance);
    }

    protected boolean shouldSwitchSound()
    {
        return this.beeInstance.func_233678_J__();
    }
}

package net.minecraft.client.audio;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;

public abstract class LocatableSound implements ISound
{
    protected Sound sound;
    protected final SoundCategory category;
    protected final ResourceLocation positionedSoundLocation;
    protected float volume = 1.0F;
    protected float pitch = 1.0F;
    protected double x;
    protected double y;
    protected double z;
    protected boolean repeat;
    protected int repeatDelay;
    protected ISound.AttenuationType attenuationType = ISound.AttenuationType.LINEAR;
    protected boolean priority;
    protected boolean global;

    protected LocatableSound(SoundEvent p_i1599_1_, SoundCategory p_i1599_2_)
    {
        this(p_i1599_1_.getName(), p_i1599_2_);
    }

    protected LocatableSound(ResourceLocation p_i1600_1_, SoundCategory p_i1600_2_)
    {
        this.positionedSoundLocation = p_i1600_1_;
        this.category = p_i1600_2_;
    }

    public ResourceLocation getSoundLocation()
    {
        return this.positionedSoundLocation;
    }

    public SoundEventAccessor createAccessor(SoundHandler handler)
    {
        SoundEventAccessor soundeventaccessor = handler.getAccessor(this.positionedSoundLocation);

        if (soundeventaccessor == null)
        {
            this.sound = SoundHandler.MISSING_SOUND;
        }
        else
        {
            this.sound = soundeventaccessor.cloneEntry();
        }

        return soundeventaccessor;
    }

    public Sound getSound()
    {
        return this.sound;
    }

    public SoundCategory getCategory()
    {
        return this.category;
    }

    public boolean canRepeat()
    {
        return this.repeat;
    }

    public int getRepeatDelay()
    {
        return this.repeatDelay;
    }

    public float getVolume()
    {
        return this.volume * this.sound.getVolume();
    }

    public float getPitch()
    {
        return this.pitch * this.sound.getPitch();
    }

    public double getX()
    {
        return this.x;
    }

    public double getY()
    {
        return this.y;
    }

    public double getZ()
    {
        return this.z;
    }

    public ISound.AttenuationType getAttenuationType()
    {
        return this.attenuationType;
    }

    public boolean isGlobal()
    {
        return this.global;
    }

    public String toString()
    {
        return "SoundInstance[" + this.positionedSoundLocation + "]";
    }
}

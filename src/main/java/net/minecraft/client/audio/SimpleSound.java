package net.minecraft.client.audio;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;

public class SimpleSound extends LocatableSound
{
    public SimpleSound(SoundEvent p_i2056_1_, SoundCategory p_i2056_2_, float p_i2056_3_, float p_i2056_4_, BlockPos p_i2056_5_)
    {
        this(p_i2056_1_, p_i2056_2_, p_i2056_3_, p_i2056_4_, (double)p_i2056_5_.getX() + 0.5D, (double)p_i2056_5_.getY() + 0.5D, (double)p_i2056_5_.getZ() + 0.5D);
    }

    public static SimpleSound master(SoundEvent soundIn, float pitchIn)
    {
        return master(soundIn, pitchIn, 0.25F);
    }

    public static SimpleSound master(SoundEvent soundIn, float pitchIn, float volumeIn)
    {
        return new SimpleSound(soundIn.getName(), SoundCategory.MASTER, volumeIn, pitchIn, false, 0, ISound.AttenuationType.NONE, 0.0D, 0.0D, 0.0D, true);
    }

    public static SimpleSound music(SoundEvent soundIn)
    {
        return new SimpleSound(soundIn.getName(), SoundCategory.MUSIC, 1.0F, 1.0F, false, 0, ISound.AttenuationType.NONE, 0.0D, 0.0D, 0.0D, true);
    }

    public static SimpleSound record(SoundEvent soundIn, double xIn, double zIn, double p_184372_5_)
    {
        return new SimpleSound(soundIn, SoundCategory.RECORDS, 4.0F, 1.0F, false, 0, ISound.AttenuationType.LINEAR, xIn, zIn, p_184372_5_);
    }

    public static SimpleSound func_239532_b_(SoundEvent p_239532_0_, float p_239532_1_, float p_239532_2_)
    {
        return new SimpleSound(p_239532_0_.getName(), SoundCategory.AMBIENT, p_239532_2_, p_239532_1_, false, 0, ISound.AttenuationType.NONE, 0.0D, 0.0D, 0.0D, true);
    }

    public static SimpleSound func_239530_b_(SoundEvent p_239530_0_)
    {
        return func_239532_b_(p_239530_0_, 1.0F, 1.0F);
    }

    public static SimpleSound func_239531_b_(SoundEvent p_239531_0_, double p_239531_1_, double p_239531_3_, double p_239531_5_)
    {
        return new SimpleSound(p_239531_0_, SoundCategory.AMBIENT, 1.0F, 1.0F, false, 0, ISound.AttenuationType.LINEAR, p_239531_1_, p_239531_3_, p_239531_5_);
    }

    public SimpleSound(SoundEvent p_i2057_1_, SoundCategory p_i2057_2_, float p_i2057_3_, float p_i2057_4_, double p_i2057_5_, double p_i2057_7_, double p_i2057_9_)
    {
        this(p_i2057_1_, p_i2057_2_, p_i2057_3_, p_i2057_4_, false, 0, ISound.AttenuationType.LINEAR, p_i2057_5_, p_i2057_7_, p_i2057_9_);
    }

    private SimpleSound(SoundEvent p_i2058_1_, SoundCategory p_i2058_2_, float p_i2058_3_, float p_i2058_4_, boolean p_i2058_5_, int p_i2058_6_, ISound.AttenuationType p_i2058_7_, double p_i2058_8_, double p_i2058_10_, double p_i2058_12_)
    {
        this(p_i2058_1_.getName(), p_i2058_2_, p_i2058_3_, p_i2058_4_, p_i2058_5_, p_i2058_6_, p_i2058_7_, p_i2058_8_, p_i2058_10_, p_i2058_12_, false);
    }

    public SimpleSound(ResourceLocation p_i2059_1_, SoundCategory p_i2059_2_, float p_i2059_3_, float p_i2059_4_, boolean p_i2059_5_, int p_i2059_6_, ISound.AttenuationType p_i2059_7_, double p_i2059_8_, double p_i2059_10_, double p_i2059_12_, boolean p_i2059_14_)
    {
        super(p_i2059_1_, p_i2059_2_);
        this.volume = p_i2059_3_;
        this.pitch = p_i2059_4_;
        this.x = p_i2059_8_;
        this.y = p_i2059_10_;
        this.z = p_i2059_12_;
        this.repeat = p_i2059_5_;
        this.repeatDelay = p_i2059_6_;
        this.attenuationType = p_i2059_7_;
        this.global = p_i2059_14_;
    }
}

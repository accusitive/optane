package net.minecraft.client.gui.screen;

import net.minecraft.client.GameSettings;
import net.minecraft.util.text.ITextComponent;

public class SettingsScreen extends Screen
{
    protected final Screen parentScreen;
    protected final GameSettings gameSettings;

    public SettingsScreen(Screen p_i3460_1_, GameSettings p_i3460_2_, ITextComponent p_i3460_3_)
    {
        super(p_i3460_3_);
        this.parentScreen = p_i3460_1_;
        this.gameSettings = p_i3460_2_;
    }

    public void removed()
    {
        this.minecraft.gameSettings.saveOptions();
    }

    public void onClose()
    {
        this.minecraft.displayGuiScreen(this.parentScreen);
    }
}

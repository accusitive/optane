package net.minecraft.client.gui.fonts;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.util.math.vector.Matrix4f;
import net.optifine.util.MathUtils;

public class TexturedGlyph
{
    private final RenderType normalType;
    private final RenderType seeThroughType;
    private final float u0;
    private final float u1;
    private final float v0;
    private final float v1;
    private final float minX;
    private final float maxX;
    private final float minY;
    private final float maxY;
    public static final Matrix4f MATRIX_IDENTITY = MathUtils.makeMatrixIdentity();

    public TexturedGlyph(RenderType p_i21_1_, RenderType p_i21_2_, float p_i21_3_, float p_i21_4_, float p_i21_5_, float p_i21_6_, float p_i21_7_, float p_i21_8_, float p_i21_9_, float p_i21_10_)
    {
        this.normalType = p_i21_1_;
        this.seeThroughType = p_i21_2_;
        this.u0 = p_i21_3_;
        this.u1 = p_i21_4_;
        this.v0 = p_i21_5_;
        this.v1 = p_i21_6_;
        this.minX = p_i21_7_;
        this.maxX = p_i21_8_;
        this.minY = p_i21_9_;
        this.maxY = p_i21_10_;
    }

    public void render(boolean italicIn, float xIn, float yIn, Matrix4f matrixIn, IVertexBuilder bufferIn, float redIn, float greenIn, float blueIn, float alphaIn, int packedLight)
    {
        int i = 3;
        float f = xIn + this.minX;
        float f1 = xIn + this.maxX;
        float f2 = this.minY - 3.0F;
        float f3 = this.maxY - 3.0F;
        float f4 = yIn + f2;
        float f5 = yIn + f3;
        float f6 = italicIn ? 1.0F - 0.25F * f2 : 0.0F;
        float f7 = italicIn ? 1.0F - 0.25F * f3 : 0.0F;

        if (bufferIn instanceof BufferBuilder && matrixIn == MATRIX_IDENTITY)
        {
            BufferBuilder bufferbuilder = (BufferBuilder)bufferIn;
            int j = (int)(redIn * 255.0F);
            int k = (int)(greenIn * 255.0F);
            int l = (int)(blueIn * 255.0F);
            int i1 = (int)(alphaIn * 255.0F);
            int j1 = packedLight & 65535;
            int k1 = packedLight >> 16 & 65535;
            bufferbuilder.addVertexText(f + f6, f4, 0.0F, j, k, l, i1, this.u0, this.v0, j1, k1);
            bufferbuilder.addVertexText(f + f7, f5, 0.0F, j, k, l, i1, this.u0, this.v1, j1, k1);
            bufferbuilder.addVertexText(f1 + f7, f5, 0.0F, j, k, l, i1, this.u1, this.v1, j1, k1);
            bufferbuilder.addVertexText(f1 + f6, f4, 0.0F, j, k, l, i1, this.u1, this.v0, j1, k1);
        }
        else
        {
            bufferIn.pos(matrixIn, f + f6, f4, 0.0F).color(redIn, greenIn, blueIn, alphaIn).tex(this.u0, this.v0).lightmap(packedLight).endVertex();
            bufferIn.pos(matrixIn, f + f7, f5, 0.0F).color(redIn, greenIn, blueIn, alphaIn).tex(this.u0, this.v1).lightmap(packedLight).endVertex();
            bufferIn.pos(matrixIn, f1 + f7, f5, 0.0F).color(redIn, greenIn, blueIn, alphaIn).tex(this.u1, this.v1).lightmap(packedLight).endVertex();
            bufferIn.pos(matrixIn, f1 + f6, f4, 0.0F).color(redIn, greenIn, blueIn, alphaIn).tex(this.u1, this.v0).lightmap(packedLight).endVertex();
        }
    }

    public void renderEffect(TexturedGlyph.Effect effectIn, Matrix4f matrixIn, IVertexBuilder bufferIn, int packedLightIn)
    {
        bufferIn.pos(matrixIn, effectIn.x0, effectIn.y0, effectIn.depth).color(effectIn.r, effectIn.g, effectIn.b, effectIn.a).tex(this.u0, this.v0).lightmap(packedLightIn).endVertex();
        bufferIn.pos(matrixIn, effectIn.x1, effectIn.y0, effectIn.depth).color(effectIn.r, effectIn.g, effectIn.b, effectIn.a).tex(this.u0, this.v1).lightmap(packedLightIn).endVertex();
        bufferIn.pos(matrixIn, effectIn.x1, effectIn.y1, effectIn.depth).color(effectIn.r, effectIn.g, effectIn.b, effectIn.a).tex(this.u1, this.v1).lightmap(packedLightIn).endVertex();
        bufferIn.pos(matrixIn, effectIn.x0, effectIn.y1, effectIn.depth).color(effectIn.r, effectIn.g, effectIn.b, effectIn.a).tex(this.u1, this.v0).lightmap(packedLightIn).endVertex();
    }

    public RenderType getRenderType(boolean seeThroughIn)
    {
        return seeThroughIn ? this.seeThroughType : this.normalType;
    }

    public static class Effect
    {
        protected final float x0;
        protected final float y0;
        protected final float x1;
        protected final float y1;
        protected final float depth;
        protected final float r;
        protected final float g;
        protected final float b;
        protected final float a;

        public Effect(float p_i156_1_, float p_i156_2_, float p_i156_3_, float p_i156_4_, float p_i156_5_, float p_i156_6_, float p_i156_7_, float p_i156_8_, float p_i156_9_)
        {
            this.x0 = p_i156_1_;
            this.y0 = p_i156_2_;
            this.x1 = p_i156_3_;
            this.y1 = p_i156_4_;
            this.depth = p_i156_5_;
            this.r = p_i156_6_;
            this.g = p_i156_7_;
            this.b = p_i156_8_;
            this.a = p_i156_9_;
        }
    }
}

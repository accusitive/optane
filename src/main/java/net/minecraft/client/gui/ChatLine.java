package net.minecraft.client.gui;

import net.minecraft.util.text.ITextProperties;

public class ChatLine
{
    private final int updateCounterCreated;
    private final ITextProperties lineString;
    private final int chatLineID;

    public ChatLine(int p_i2123_1_, ITextProperties p_i2123_2_, int p_i2123_3_)
    {
        this.lineString = p_i2123_2_;
        this.updateCounterCreated = p_i2123_1_;
        this.chatLineID = p_i2123_3_;
    }

    public ITextProperties func_238169_a_()
    {
        return this.lineString;
    }

    public int getUpdatedCounter()
    {
        return this.updateCounterCreated;
    }

    public int getChatLineID()
    {
        return this.chatLineID;
    }
}

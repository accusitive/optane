package net.minecraft.client.gui.widget.list;

import net.minecraft.client.Minecraft;

public abstract class ExtendedList<E extends AbstractList.AbstractListEntry<E>> extends AbstractList<E>
{
    private boolean field_230698_a_;

    public ExtendedList(Minecraft p_i3618_1_, int p_i3618_2_, int p_i3618_3_, int p_i3618_4_, int p_i3618_5_, int p_i3618_6_)
    {
        super(p_i3618_1_, p_i3618_2_, p_i3618_3_, p_i3618_4_, p_i3618_5_, p_i3618_6_);
    }

    public boolean changeFocus(boolean p_231049_1_)
    {
        if (!this.field_230698_a_ && this.getItemCount() == 0)
        {
            return false;
        }
        else
        {
            this.field_230698_a_ = !this.field_230698_a_;

            if (this.field_230698_a_ && this.getSelected() == null && this.getItemCount() > 0)
            {
                this.func_241219_a_(AbstractList.Ordering.DOWN);
            }
            else if (this.field_230698_a_ && this.getSelected() != null)
            {
                this.func_241574_n_();
            }

            return this.field_230698_a_;
        }
    }

    public abstract static class AbstractListEntry<E extends ExtendedList.AbstractListEntry<E>> extends AbstractList.AbstractListEntry<E>
    {
        public boolean changeFocus(boolean p_231049_1_)
        {
            return false;
        }
    }
}

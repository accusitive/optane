package net.minecraft.client.gui.widget.button;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;

public class ImageButton extends Button
{
    private final ResourceLocation resourceLocation;
    private final int xTexStart;
    private final int yTexStart;
    private final int yDiffText;
    private final int textureWidth;
    private final int textureHeight;

    public ImageButton(int p_i2404_1_, int p_i2404_2_, int p_i2404_3_, int p_i2404_4_, int p_i2404_5_, int p_i2404_6_, int p_i2404_7_, ResourceLocation p_i2404_8_, Button.IPressable p_i2404_9_)
    {
        this(p_i2404_1_, p_i2404_2_, p_i2404_3_, p_i2404_4_, p_i2404_5_, p_i2404_6_, p_i2404_7_, p_i2404_8_, 256, 256, p_i2404_9_);
    }

    public ImageButton(int p_i2405_1_, int p_i2405_2_, int p_i2405_3_, int p_i2405_4_, int p_i2405_5_, int p_i2405_6_, int p_i2405_7_, ResourceLocation p_i2405_8_, int p_i2405_9_, int p_i2405_10_, Button.IPressable p_i2405_11_)
    {
        this(p_i2405_1_, p_i2405_2_, p_i2405_3_, p_i2405_4_, p_i2405_5_, p_i2405_6_, p_i2405_7_, p_i2405_8_, p_i2405_9_, p_i2405_10_, p_i2405_11_, StringTextComponent.field_240750_d_);
    }

    public ImageButton(int p_i2406_1_, int p_i2406_2_, int p_i2406_3_, int p_i2406_4_, int p_i2406_5_, int p_i2406_6_, int p_i2406_7_, ResourceLocation p_i2406_8_, int p_i2406_9_, int p_i2406_10_, Button.IPressable p_i2406_11_, ITextComponent p_i2406_12_)
    {
        super(p_i2406_1_, p_i2406_2_, p_i2406_3_, p_i2406_4_, p_i2406_12_, p_i2406_11_);
        this.textureWidth = p_i2406_9_;
        this.textureHeight = p_i2406_10_;
        this.xTexStart = p_i2406_5_;
        this.yTexStart = p_i2406_6_;
        this.yDiffText = p_i2406_7_;
        this.resourceLocation = p_i2406_8_;
    }

    public void setPosition(int xIn, int yIn)
    {
        this.x = xIn;
        this.y = yIn;
    }

    public void renderButton(MatrixStack p_230431_1_, int p_230431_2_, int p_230431_3_, float p_230431_4_)
    {
        Minecraft minecraft = Minecraft.getInstance();
        minecraft.getTextureManager().bindTexture(this.resourceLocation);
        int i = this.yTexStart;

        if (this.isHovered())
        {
            i += this.yDiffText;
        }

        RenderSystem.enableDepthTest();
        blit(p_230431_1_, this.x, this.y, (float)this.xTexStart, (float)i, this.width, this.height, this.textureWidth, this.textureHeight);
    }
}

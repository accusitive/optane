package net.minecraft.client.gui.widget;

import net.minecraft.client.AbstractOption;
import net.minecraft.client.GameSettings;
import net.minecraft.client.settings.SliderPercentageOption;
import net.optifine.gui.IOptionControl;

public class OptionSlider extends GameSettingsSlider implements IOptionControl
{
    private final SliderPercentageOption option;

    public OptionSlider(GameSettings p_i181_1_, int p_i181_2_, int p_i181_3_, int p_i181_4_, int p_i181_5_, SliderPercentageOption p_i181_6_)
    {
        super(p_i181_1_, p_i181_2_, p_i181_3_, p_i181_4_, p_i181_5_, (double)((float)p_i181_6_.normalizeValue(p_i181_6_.get(p_i181_1_))));
        this.option = p_i181_6_;
        this.func_230979_b_();
    }

    protected void func_230972_a_()
    {
        this.option.set(this.field_238477_a_, this.option.denormalizeValue(this.field_230683_b_));
        this.field_238477_a_.saveOptions();
    }

    protected void func_230979_b_()
    {
        this.setMessage(this.option.func_238334_c_(this.field_238477_a_));
    }

    public static int getWidth(Widget p_getWidth_0_)
    {
        return p_getWidth_0_.width;
    }

    public static int getHeight(Widget p_getHeight_0_)
    {
        return p_getHeight_0_.height;
    }

    public AbstractOption getControlOption()
    {
        return this.option;
    }
}

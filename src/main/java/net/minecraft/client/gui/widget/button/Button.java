package net.minecraft.client.gui.widget.button;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.util.text.ITextComponent;

public class Button extends AbstractButton
{
    public static final Button.ITooltip field_238486_s_ = (p_238488_0_, p_238488_1_, p_238488_2_, p_238488_3_) ->
    {
    };
    protected final Button.IPressable field_230697_t_;
    protected final Button.ITooltip field_238487_u_;

    public Button(int languagesIn, int p_i1311_2_, int p_i1311_3_, int p_i1311_4_, ITextComponent p_i1311_5_, Button.IPressable p_i1311_6_)
    {
        this(languagesIn, p_i1311_2_, p_i1311_3_, p_i1311_4_, p_i1311_5_, p_i1311_6_, field_238486_s_);
    }

    public Button(int p_i1312_1_, int p_i1312_2_, int p_i1312_3_, int p_i1312_4_, ITextComponent p_i1312_5_, Button.IPressable p_i1312_6_, Button.ITooltip p_i1312_7_)
    {
        super(p_i1312_1_, p_i1312_2_, p_i1312_3_, p_i1312_4_, p_i1312_5_);
        this.field_230697_t_ = p_i1312_6_;
        this.field_238487_u_ = p_i1312_7_;
    }

    public void onPress()
    {
        this.field_230697_t_.onPress(this);
    }

    public void renderButton(MatrixStack p_230431_1_, int p_230431_2_, int p_230431_3_, float p_230431_4_)
    {
        super.renderButton(p_230431_1_, p_230431_2_, p_230431_3_, p_230431_4_);

        if (this.isHovered())
        {
            this.renderToolTip(p_230431_1_, p_230431_2_, p_230431_3_);
        }
    }

    public void renderToolTip(MatrixStack p_230443_1_, int p_230443_2_, int p_230443_3_)
    {
        this.field_238487_u_.onTooltip(this, p_230443_1_, p_230443_2_, p_230443_3_);
    }

    public interface IPressable
    {
        void onPress(Button p_onPress_1_);
    }

    public interface ITooltip
    {
        void onTooltip(Button p_onTooltip_1_, MatrixStack p_onTooltip_2_, int p_onTooltip_3_, int p_onTooltip_4_);
    }
}

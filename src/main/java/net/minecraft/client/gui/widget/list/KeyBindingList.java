package net.minecraft.client.gui.widget.list;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.matrix.MatrixStack;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.IGuiEventListener;
import net.minecraft.client.gui.screen.ControlsScreen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import org.apache.commons.lang3.ArrayUtils;

public class KeyBindingList extends AbstractOptionList<KeyBindingList.Entry>
{
    private final ControlsScreen controlsScreen;
    private int maxListLabelWidth;

    public KeyBindingList(ControlsScreen p_i3055_1_, Minecraft p_i3055_2_)
    {
        super(p_i3055_2_, p_i3055_1_.width + 45, p_i3055_1_.height, 43, p_i3055_1_.height - 32, 20);
        this.controlsScreen = p_i3055_1_;
        KeyBinding[] akeybinding = ArrayUtils.clone(p_i3055_2_.gameSettings.keyBindings);
        Arrays.sort((Object[])akeybinding);
        String s = null;

        for (KeyBinding keybinding : akeybinding)
        {
            String s1 = keybinding.getKeyCategory();

            if (!s1.equals(s))
            {
                s = s1;
                this.addEntry(new KeyBindingList.CategoryEntry(new TranslationTextComponent(s1)));
            }

            ITextComponent itextcomponent = new TranslationTextComponent(keybinding.getKeyDescription());
            int i = p_i3055_2_.fontRenderer.func_238414_a_(itextcomponent);

            if (i > this.maxListLabelWidth)
            {
                this.maxListLabelWidth = i;
            }

            this.addEntry(new KeyBindingList.KeyEntry(keybinding, itextcomponent));
        }
    }

    protected int getScrollbarPosition()
    {
        return super.getScrollbarPosition() + 15;
    }

    public int getRowWidth()
    {
        return super.getRowWidth() + 32;
    }

    public class CategoryEntry extends KeyBindingList.Entry
    {
        private final ITextComponent labelText;
        private final int labelWidth;

        public CategoryEntry(ITextComponent p_i630_2_)
        {
            this.labelText = p_i630_2_;
            this.labelWidth = KeyBindingList.this.minecraft.fontRenderer.func_238414_a_(this.labelText);
        }

        public void render(MatrixStack p_230432_1_, int p_230432_2_, int p_230432_3_, int p_230432_4_, int p_230432_5_, int p_230432_6_, int p_230432_7_, int p_230432_8_, boolean p_230432_9_, float p_230432_10_)
        {
            KeyBindingList.this.minecraft.fontRenderer.drawString(p_230432_1_, this.labelText, (float)(KeyBindingList.this.minecraft.currentScreen.width / 2 - this.labelWidth / 2), (float)(p_230432_3_ + p_230432_6_ - 9 - 1), 16777215);
        }

        public boolean changeFocus(boolean p_231049_1_)
        {
            return false;
        }

        public List <? extends IGuiEventListener > children()
        {
            return Collections.emptyList();
        }
    }

    public abstract static class Entry extends AbstractOptionList.Entry<KeyBindingList.Entry>
    {
    }

    public class KeyEntry extends KeyBindingList.Entry
    {
        private final KeyBinding keybinding;
        private final ITextComponent keyDesc;
        private final Button btnChangeKeyBinding;
        private final Button btnReset;

        private KeyEntry(final KeyBinding p_i3005_2_, final ITextComponent p_i3005_3_)
        {
            this.keybinding = p_i3005_2_;
            this.keyDesc = p_i3005_3_;
            this.btnChangeKeyBinding = new Button(0, 0, 75, 20, p_i3005_3_, (p_214386_2_) ->
            {
                KeyBindingList.this.controlsScreen.buttonId = p_i3005_2_;
            })
            {
                protected IFormattableTextComponent func_230442_c_()
                {
                    return p_i3005_2_.isInvalid() ? new TranslationTextComponent("narrator.controls.unbound", p_i3005_3_) : new TranslationTextComponent("narrator.controls.bound", p_i3005_3_, super.func_230442_c_());
                }
            };
            this.btnReset = new Button(0, 0, 50, 20, new TranslationTextComponent("controls.reset"), (p_214387_2_) ->
            {
                KeyBindingList.this.minecraft.gameSettings.setKeyBindingCode(p_i3005_2_, p_i3005_2_.getDefault());
                KeyBinding.resetKeyBindingArrayAndHash();
            })
            {
                protected IFormattableTextComponent func_230442_c_()
                {
                    return new TranslationTextComponent("narrator.controls.reset", p_i3005_3_);
                }
            };
        }

        public void render(MatrixStack p_230432_1_, int p_230432_2_, int p_230432_3_, int p_230432_4_, int p_230432_5_, int p_230432_6_, int p_230432_7_, int p_230432_8_, boolean p_230432_9_, float p_230432_10_)
        {
            boolean flag = KeyBindingList.this.controlsScreen.buttonId == this.keybinding;
            KeyBindingList.this.minecraft.fontRenderer.drawString(p_230432_1_, this.keyDesc, (float)(p_230432_4_ + 90 - KeyBindingList.this.maxListLabelWidth), (float)(p_230432_3_ + p_230432_6_ / 2 - 9 / 2), 16777215);
            this.btnReset.x = p_230432_4_ + 190;
            this.btnReset.y = p_230432_3_;
            this.btnReset.active = !this.keybinding.isDefault();
            this.btnReset.render(p_230432_1_, p_230432_7_, p_230432_8_, p_230432_10_);
            this.btnChangeKeyBinding.x = p_230432_4_ + 105;
            this.btnChangeKeyBinding.y = p_230432_3_;
            this.btnChangeKeyBinding.setMessage(this.keybinding.func_238171_j_());
            boolean flag1 = false;

            if (!this.keybinding.isInvalid())
            {
                for (KeyBinding keybinding : KeyBindingList.this.minecraft.gameSettings.keyBindings)
                {
                    if (keybinding != this.keybinding && this.keybinding.conflicts(keybinding))
                    {
                        flag1 = true;
                        break;
                    }
                }
            }

            if (flag)
            {
                this.btnChangeKeyBinding.setMessage((new StringTextComponent("> ")).func_230529_a_(this.btnChangeKeyBinding.getMessage().func_230532_e_().func_240699_a_(TextFormatting.YELLOW)).func_240702_b_(" <").func_240699_a_(TextFormatting.YELLOW));
            }
            else if (flag1)
            {
                this.btnChangeKeyBinding.setMessage(this.btnChangeKeyBinding.getMessage().func_230532_e_().func_240699_a_(TextFormatting.RED));
            }

            this.btnChangeKeyBinding.render(p_230432_1_, p_230432_7_, p_230432_8_, p_230432_10_);
        }

        public List <? extends IGuiEventListener > children()
        {
            return ImmutableList.of(this.btnChangeKeyBinding, this.btnReset);
        }

        public boolean mouseClicked(double p_231044_1_, double p_231044_3_, int p_231044_5_)
        {
            if (this.btnChangeKeyBinding.mouseClicked(p_231044_1_, p_231044_3_, p_231044_5_))
            {
                return true;
            }
            else
            {
                return this.btnReset.mouseClicked(p_231044_1_, p_231044_3_, p_231044_5_);
            }
        }

        public boolean mouseReleased(double p_231048_1_, double p_231048_3_, int p_231048_5_)
        {
            return this.btnChangeKeyBinding.mouseReleased(p_231048_1_, p_231048_3_, p_231048_5_) || this.btnReset.mouseReleased(p_231048_1_, p_231048_3_, p_231048_5_);
        }
    }
}

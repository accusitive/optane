package net.minecraft.client.gui.widget.button;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.util.text.ITextComponent;

public abstract class AbstractButton extends Widget
{
    public AbstractButton(int p_i3017_1_, int p_i3017_2_, int p_i3017_3_, int p_i3017_4_, ITextComponent p_i3017_5_)
    {
        super(p_i3017_1_, p_i3017_2_, p_i3017_3_, p_i3017_4_, p_i3017_5_);
    }

    public abstract void onPress();

    public void onClick(double p_230982_1_, double p_230982_3_)
    {
        this.onPress();
    }

    public boolean keyPressed(int p_231046_1_, int p_231046_2_, int p_231046_3_)
    {
        if (this.active && this.visible)
        {
            if (p_231046_1_ != 257 && p_231046_1_ != 32 && p_231046_1_ != 335)
            {
                return false;
            }
            else
            {
                this.playDownSound(Minecraft.getInstance().getSoundHandler());
                this.onPress();
                return true;
            }
        }
        else
        {
            return false;
        }
    }
}

package net.minecraft.client.gui.widget.list;

import javax.annotation.Nullable;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.IGuiEventListener;
import net.minecraft.client.gui.INestedGuiEventHandler;

public abstract class AbstractOptionList<E extends AbstractOptionList.Entry<E>> extends AbstractList<E>
{
    public AbstractOptionList(Minecraft p_i837_1_, int p_i837_2_, int p_i837_3_, int p_i837_4_, int p_i837_5_, int p_i837_6_)
    {
        super(p_i837_1_, p_i837_2_, p_i837_3_, p_i837_4_, p_i837_5_, p_i837_6_);
    }

    public boolean changeFocus(boolean p_231049_1_)
    {
        boolean flag = super.changeFocus(p_231049_1_);

        if (flag)
        {
            this.ensureVisible(this.getFocused());
        }

        return flag;
    }

    protected boolean isSelectedItem(int p_230957_1_)
    {
        return false;
    }

    public abstract static class Entry<E extends AbstractOptionList.Entry<E>> extends AbstractList.AbstractListEntry<E> implements INestedGuiEventHandler
    {
        @Nullable
        private IGuiEventListener field_214380_a;
        private boolean field_214381_b;

        public boolean func_231041_ay__()
        {
            return this.field_214381_b;
        }

        public void func_231037_b__(boolean p_231037_1_)
        {
            this.field_214381_b = p_231037_1_;
        }

        public void func_231035_a_(@Nullable IGuiEventListener p_231035_1_)
        {
            this.field_214380_a = p_231035_1_;
        }

        @Nullable
        public IGuiEventListener getFocused()
        {
            return this.field_214380_a;
        }
    }
}

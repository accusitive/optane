package net.minecraft.client.gui.widget;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.DialogTexts;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;

public class SoundSlider extends GameSettingsSlider
{
    private final SoundCategory category;

    public SoundSlider(Minecraft p_i382_1_, int p_i382_2_, int p_i382_3_, SoundCategory p_i382_4_, int p_i382_5_)
    {
        super(p_i382_1_.gameSettings, p_i382_2_, p_i382_3_, p_i382_5_, 20, (double)p_i382_1_.gameSettings.getSoundLevel(p_i382_4_));
        this.category = p_i382_4_;
        this.func_230979_b_();
    }

    protected void func_230979_b_()
    {
        ITextComponent itextcomponent = (ITextComponent)((float)this.field_230683_b_ == (float)this.getYImage(false) ? DialogTexts.field_240631_b_ : new StringTextComponent((int)(this.field_230683_b_ * 100.0D) + "%"));
        this.setMessage((new TranslationTextComponent("soundCategory." + this.category.getName())).func_240702_b_(": ").func_230529_a_(itextcomponent));
    }

    protected void func_230972_a_()
    {
        this.field_238477_a_.setSoundLevel(this.category, (float)this.field_230683_b_);
        this.field_238477_a_.saveOptions();
    }
}

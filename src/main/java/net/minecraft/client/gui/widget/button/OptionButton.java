package net.minecraft.client.gui.widget.button;

import net.minecraft.client.AbstractOption;
import net.minecraft.util.text.ITextComponent;
import net.optifine.gui.IOptionControl;

public class OptionButton extends Button implements IOptionControl
{
    private final AbstractOption enumOptions;

    public OptionButton(int p_i251_1_, int p_i251_2_, int p_i251_3_, int p_i251_4_, AbstractOption p_i251_5_, ITextComponent p_i251_6_, Button.IPressable p_i251_7_)
    {
        super(p_i251_1_, p_i251_2_, p_i251_3_, p_i251_4_, p_i251_6_, p_i251_7_);
        this.enumOptions = p_i251_5_;
    }

    public AbstractOption func_238517_a_()
    {
        return this.enumOptions;
    }

    public AbstractOption getControlOption()
    {
        return this.enumOptions;
    }
}

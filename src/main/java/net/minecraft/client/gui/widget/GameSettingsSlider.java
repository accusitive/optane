package net.minecraft.client.gui.widget;

import net.minecraft.client.GameSettings;
import net.minecraft.util.text.StringTextComponent;

public abstract class GameSettingsSlider extends AbstractSlider
{
    protected final GameSettings field_238477_a_;

    protected GameSettingsSlider(GameSettings p_i4334_1_, int p_i4334_2_, int p_i4334_3_, int p_i4334_4_, int p_i4334_5_, double p_i4334_6_)
    {
        super(p_i4334_2_, p_i4334_3_, p_i4334_4_, p_i4334_5_, StringTextComponent.field_240750_d_, p_i4334_6_);
        this.field_238477_a_ = p_i4334_1_;
    }
}

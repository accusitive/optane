package net.minecraft.client.util;

public class UndeclaredException extends RuntimeException
{
    public UndeclaredException(String p_i852_1_)
    {
        super(p_i852_1_);
    }
}

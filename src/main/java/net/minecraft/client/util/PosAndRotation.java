package net.minecraft.client.util;

import java.util.Objects;
import net.minecraft.util.math.vector.Vector3d;

public class PosAndRotation
{
    private final Vector3d pos;
    private final float pitch;
    private final float yaw;

    public PosAndRotation(Vector3d p_i769_1_, float p_i769_2_, float p_i769_3_)
    {
        this.pos = p_i769_1_;
        this.pitch = p_i769_2_;
        this.yaw = p_i769_3_;
    }

    public Vector3d getPos()
    {
        return this.pos;
    }

    public float getPitch()
    {
        return this.pitch;
    }

    public float getYaw()
    {
        return this.yaw;
    }

    public boolean equals(Object p_equals_1_)
    {
        if (this == p_equals_1_)
        {
            return true;
        }
        else if (p_equals_1_ != null && this.getClass() == p_equals_1_.getClass())
        {
            PosAndRotation posandrotation = (PosAndRotation)p_equals_1_;
            return Float.compare(posandrotation.pitch, this.pitch) == 0 && Float.compare(posandrotation.yaw, this.yaw) == 0 && Objects.equals(this.pos, posandrotation.pos);
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash(this.pos, this.pitch, this.yaw);
    }

    public String toString()
    {
        return "PosAndRot[" + this.pos + " (" + this.pitch + ", " + this.yaw + ")]";
    }
}

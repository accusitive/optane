package net.minecraft.client.particle;

import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.particles.IParticleData;
import net.minecraft.util.math.vector.Vector3d;

public class EmitterParticle extends MetaParticle
{
    private final Entity attachedEntity;
    private int age;
    private final int lifetime;
    private final IParticleData particleTypes;

    public EmitterParticle(ClientWorld p_i4146_1_, Entity p_i4146_2_, IParticleData p_i4146_3_)
    {
        this(p_i4146_1_, p_i4146_2_, p_i4146_3_, 3);
    }

    public EmitterParticle(ClientWorld p_i4147_1_, Entity p_i4147_2_, IParticleData p_i4147_3_, int p_i4147_4_)
    {
        this(p_i4147_1_, p_i4147_2_, p_i4147_3_, p_i4147_4_, p_i4147_2_.getMotion());
    }

    private EmitterParticle(ClientWorld p_i4148_1_, Entity p_i4148_2_, IParticleData p_i4148_3_, int p_i4148_4_, Vector3d p_i4148_5_)
    {
        super(p_i4148_1_, p_i4148_2_.getPosX(), p_i4148_2_.getPosYHeight(0.5D), p_i4148_2_.getPosZ(), p_i4148_5_.x, p_i4148_5_.y, p_i4148_5_.z);
        this.attachedEntity = p_i4148_2_;
        this.lifetime = p_i4148_4_;
        this.particleTypes = p_i4148_3_;
        this.tick();
    }

    public void tick()
    {
        for (int i = 0; i < 16; ++i)
        {
            double d0 = (double)(this.rand.nextFloat() * 2.0F - 1.0F);
            double d1 = (double)(this.rand.nextFloat() * 2.0F - 1.0F);
            double d2 = (double)(this.rand.nextFloat() * 2.0F - 1.0F);

            if (!(d0 * d0 + d1 * d1 + d2 * d2 > 1.0D))
            {
                double d3 = this.attachedEntity.getPosXWidth(d0 / 4.0D);
                double d4 = this.attachedEntity.getPosYHeight(0.5D + d1 / 4.0D);
                double d5 = this.attachedEntity.getPosZWidth(d2 / 4.0D);
                this.world.addParticle(this.particleTypes, false, d3, d4, d5, d0, d1 + 0.2D, d2);
            }
        }

        ++this.age;

        if (this.age >= this.lifetime)
        {
            this.setExpired();
        }
    }
}

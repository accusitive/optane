package net.minecraft.client.particle;

import net.minecraft.client.world.ClientWorld;
import net.minecraft.particles.BasicParticleType;

public class SplashParticle extends RainParticle
{
    private SplashParticle(ClientWorld p_i942_1_, double p_i942_2_, double p_i942_4_, double p_i942_6_, double p_i942_8_, double p_i942_10_, double p_i942_12_)
    {
        super(p_i942_1_, p_i942_2_, p_i942_4_, p_i942_6_);
        this.particleGravity = 0.04F;

        if (p_i942_10_ == 0.0D && (p_i942_8_ != 0.0D || p_i942_12_ != 0.0D))
        {
            this.motionX = p_i942_8_;
            this.motionY = 0.1D;
            this.motionZ = p_i942_12_;
        }
    }

    public static class Factory implements IParticleFactory<BasicParticleType>
    {
        private final IAnimatedSprite spriteSet;

        public Factory(IAnimatedSprite p_i2521_1_)
        {
            this.spriteSet = p_i2521_1_;
        }

        public Particle makeParticle(BasicParticleType typeIn, ClientWorld worldIn, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed)
        {
            SplashParticle splashparticle = new SplashParticle(worldIn, x, y, z, xSpeed, ySpeed, zSpeed);
            splashparticle.selectSpriteRandomly(this.spriteSet);
            return splashparticle;
        }
    }
}

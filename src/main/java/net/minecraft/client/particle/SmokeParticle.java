package net.minecraft.client.particle;

import net.minecraft.client.world.ClientWorld;
import net.minecraft.particles.BasicParticleType;

public class SmokeParticle extends RisingParticle
{
    protected SmokeParticle(ClientWorld p_i624_1_, double p_i624_2_, double p_i624_4_, double p_i624_6_, double p_i624_8_, double p_i624_10_, double p_i624_12_, float p_i624_14_, IAnimatedSprite p_i624_15_)
    {
        super(p_i624_1_, p_i624_2_, p_i624_4_, p_i624_6_, 0.1F, 0.1F, 0.1F, p_i624_8_, p_i624_10_, p_i624_12_, p_i624_14_, p_i624_15_, 0.3F, 8, 0.004D, true);
    }

    public static class Factory implements IParticleFactory<BasicParticleType>
    {
        private final IAnimatedSprite spriteSet;

        public Factory(IAnimatedSprite p_i3834_1_)
        {
            this.spriteSet = p_i3834_1_;
        }

        public Particle makeParticle(BasicParticleType typeIn, ClientWorld worldIn, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed)
        {
            return new SmokeParticle(worldIn, x, y, z, xSpeed, ySpeed, zSpeed, 1.0F, this.spriteSet);
        }
    }
}

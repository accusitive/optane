package net.minecraft.client.particle;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.world.ClientWorld;

public class MetaParticle extends Particle
{
    protected MetaParticle(ClientWorld namespaceIn, double pathIn, double p_i1292_4_, double p_i1292_6_)
    {
        super(namespaceIn, pathIn, p_i1292_4_, p_i1292_6_);
    }

    protected MetaParticle(ClientWorld resourceName, double p_i1293_2_, double p_i1293_4_, double p_i1293_6_, double p_i1293_8_, double p_i1293_10_, double p_i1293_12_)
    {
        super(resourceName, p_i1293_2_, p_i1293_4_, p_i1293_6_, p_i1293_8_, p_i1293_10_, p_i1293_12_);
    }

    public final void renderParticle(IVertexBuilder buffer, ActiveRenderInfo renderInfo, float partialTicks)
    {
    }

    public IParticleRenderType getRenderType()
    {
        return IParticleRenderType.NO_RENDER;
    }
}

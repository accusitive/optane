package net.minecraft.client.particle;

import net.minecraft.client.world.ClientWorld;
import net.minecraft.particles.BasicParticleType;

public class SoulParticle extends DeceleratingParticle
{
    private final IAnimatedSprite field_239197_a_;

    private SoulParticle(ClientWorld p_i3518_1_, double p_i3518_2_, double p_i3518_4_, double p_i3518_6_, double p_i3518_8_, double p_i3518_10_, double p_i3518_12_, IAnimatedSprite p_i3518_14_)
    {
        super(p_i3518_1_, p_i3518_2_, p_i3518_4_, p_i3518_6_, p_i3518_8_, p_i3518_10_, p_i3518_12_);
        this.field_239197_a_ = p_i3518_14_;
        this.multiplyParticleScaleBy(1.5F);
        this.selectSpriteWithAge(p_i3518_14_);
    }

    public IParticleRenderType getRenderType()
    {
        return IParticleRenderType.PARTICLE_SHEET_TRANSLUCENT;
    }

    public void tick()
    {
        super.tick();

        if (!this.isExpired)
        {
            this.selectSpriteWithAge(this.field_239197_a_);
        }
    }

    public static class Factory implements IParticleFactory<BasicParticleType>
    {
        private final IAnimatedSprite field_239198_a_;

        public Factory(IAnimatedSprite p_i386_1_)
        {
            this.field_239198_a_ = p_i386_1_;
        }

        public Particle makeParticle(BasicParticleType typeIn, ClientWorld worldIn, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed)
        {
            SoulParticle soulparticle = new SoulParticle(worldIn, x, y, z, xSpeed, ySpeed, zSpeed, this.field_239198_a_);
            soulparticle.setAlphaF(1.0F);
            soulparticle.selectSpriteRandomly(this.field_239198_a_);
            return soulparticle;
        }
    }
}

package net.minecraft.client.particle;

import net.minecraft.client.world.ClientWorld;
import net.minecraft.particles.BasicParticleType;

public class EndRodParticle extends SimpleAnimatedParticle
{
    private EndRodParticle(ClientWorld p_i429_1_, double p_i429_2_, double p_i429_4_, double p_i429_6_, double p_i429_8_, double p_i429_10_, double p_i429_12_, IAnimatedSprite p_i429_14_)
    {
        super(p_i429_1_, p_i429_2_, p_i429_4_, p_i429_6_, p_i429_14_, -5.0E-4F);
        this.motionX = p_i429_8_;
        this.motionY = p_i429_10_;
        this.motionZ = p_i429_12_;
        this.particleScale *= 0.75F;
        this.maxAge = 60 + this.rand.nextInt(12);
        this.setColorFade(15916745);
        this.selectSpriteWithAge(p_i429_14_);
    }

    public void move(double x, double y, double z)
    {
        this.setBoundingBox(this.getBoundingBox().offset(x, y, z));
        this.resetPositionToBB();
    }

    public static class Factory implements IParticleFactory<BasicParticleType>
    {
        private final IAnimatedSprite spriteSet;

        public Factory(IAnimatedSprite p_i4365_1_)
        {
            this.spriteSet = p_i4365_1_;
        }

        public Particle makeParticle(BasicParticleType typeIn, ClientWorld worldIn, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed)
        {
            return new EndRodParticle(worldIn, x, y, z, xSpeed, ySpeed, zSpeed, this.spriteSet);
        }
    }
}

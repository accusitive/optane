package net.minecraft.client.particle;

import net.minecraft.client.world.ClientWorld;
import net.minecraft.particles.BasicParticleType;

public class SpitParticle extends PoofParticle
{
    private SpitParticle(ClientWorld p_i2002_1_, double p_i2002_2_, double p_i2002_4_, double p_i2002_6_, double p_i2002_8_, double p_i2002_10_, double p_i2002_12_, IAnimatedSprite p_i2002_14_)
    {
        super(p_i2002_1_, p_i2002_2_, p_i2002_4_, p_i2002_6_, p_i2002_8_, p_i2002_10_, p_i2002_12_, p_i2002_14_);
        this.particleGravity = 0.5F;
    }

    public void tick()
    {
        super.tick();
        this.motionY -= 0.004D + 0.04D * (double)this.particleGravity;
    }

    public static class Factory implements IParticleFactory<BasicParticleType>
    {
        private final IAnimatedSprite spriteSet;

        public Factory(IAnimatedSprite p_i1296_1_)
        {
            this.spriteSet = p_i1296_1_;
        }

        public Particle makeParticle(BasicParticleType typeIn, ClientWorld worldIn, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed)
        {
            return new SpitParticle(worldIn, x, y, z, xSpeed, ySpeed, zSpeed, this.spriteSet);
        }
    }
}

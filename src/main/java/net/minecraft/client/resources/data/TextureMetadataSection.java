package net.minecraft.client.resources.data;

public class TextureMetadataSection
{
    public static final TextureMetadataSectionSerializer SERIALIZER = new TextureMetadataSectionSerializer();
    private final boolean textureBlur;
    private final boolean textureClamp;

    public TextureMetadataSection(boolean p_i2528_1_, boolean p_i2528_2_)
    {
        this.textureBlur = p_i2528_1_;
        this.textureClamp = p_i2528_2_;
    }

    public boolean getTextureBlur()
    {
        return this.textureBlur;
    }

    public boolean getTextureClamp()
    {
        return this.textureClamp;
    }
}

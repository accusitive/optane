package net.minecraft.client.renderer.entity.model;

import net.minecraft.entity.monster.GiantEntity;

public class GiantModel extends AbstractZombieModel<GiantEntity>
{
    public GiantModel()
    {
        this(0.0F, false);
    }

    public GiantModel(float p_i1844_1_, boolean p_i1844_2_)
    {
        super(p_i1844_1_, 0.0F, 64, p_i1844_2_ ? 32 : 64);
    }

    public boolean isAggressive(GiantEntity entityIn)
    {
        return false;
    }
}

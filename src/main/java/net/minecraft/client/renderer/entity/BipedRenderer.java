package net.minecraft.client.renderer.entity;

import net.minecraft.client.renderer.entity.layers.ElytraLayer;
import net.minecraft.client.renderer.entity.layers.HeadLayer;
import net.minecraft.client.renderer.entity.layers.HeldItemLayer;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.entity.MobEntity;
import net.minecraft.util.ResourceLocation;

public class BipedRenderer<T extends MobEntity, M extends BipedModel<T>> extends MobRenderer<T, M>
{
    private static final ResourceLocation DEFAULT_RES_LOC = new ResourceLocation("textures/entity/steve.png");

    public BipedRenderer(EntityRendererManager p_i2495_1_, M p_i2495_2_, float p_i2495_3_)
    {
        this(p_i2495_1_, p_i2495_2_, p_i2495_3_, 1.0F, 1.0F, 1.0F);
    }

    public BipedRenderer(EntityRendererManager p_i2496_1_, M p_i2496_2_, float p_i2496_3_, float p_i2496_4_, float p_i2496_5_, float p_i2496_6_)
    {
        super(p_i2496_1_, p_i2496_2_, p_i2496_3_);
        this.addLayer(new HeadLayer<>(this, p_i2496_4_, p_i2496_5_, p_i2496_6_));
        this.addLayer(new ElytraLayer<>(this));
        this.addLayer(new HeldItemLayer<>(this));
    }

    public ResourceLocation getEntityTexture(T entity)
    {
        return DEFAULT_RES_LOC;
    }
}

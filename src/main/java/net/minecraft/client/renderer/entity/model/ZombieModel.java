package net.minecraft.client.renderer.entity.model;

import net.minecraft.entity.monster.ZombieEntity;

public class ZombieModel<T extends ZombieEntity> extends AbstractZombieModel<T>
{
    public ZombieModel(float p_i358_1_, boolean p_i358_2_)
    {
        this(p_i358_1_, 0.0F, 64, p_i358_2_ ? 32 : 64);
    }

    protected ZombieModel(float p_i359_1_, float p_i359_2_, int p_i359_3_, int p_i359_4_)
    {
        super(p_i359_1_, p_i359_2_, p_i359_3_, p_i359_4_);
    }

    public boolean isAggressive(T entityIn)
    {
        return entityIn.isAggressive();
    }
}

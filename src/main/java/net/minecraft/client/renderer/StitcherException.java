package net.minecraft.client.renderer;

import java.util.Collection;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;

public class StitcherException extends RuntimeException
{
    private final Collection<TextureAtlasSprite.Info> spriteInfos;

    public StitcherException(TextureAtlasSprite.Info p_i118_1_, Collection<TextureAtlasSprite.Info> p_i118_2_)
    {
        super(String.format("Unable to fit: %s - size: %dx%d - Maybe try a lower resolution resourcepack?", p_i118_1_.getSpriteLocation(), p_i118_1_.getSpriteWidth(), p_i118_1_.getSpriteHeight()));
        this.spriteInfos = p_i118_2_;
    }

    public Collection<TextureAtlasSprite.Info> getSpriteInfos()
    {
        return this.spriteInfos;
    }

    public StitcherException(TextureAtlasSprite.Info p_i119_1_, Collection<TextureAtlasSprite.Info> p_i119_2_, int p_i119_3_, int p_i119_4_, int p_i119_5_, int p_i119_6_)
    {
        super(String.format("Unable to fit: %s, size: %dx%d, atlas: %dx%d, atlasMax: %dx%d - Maybe try a lower resolution resourcepack?", "" + p_i119_1_.getSpriteLocation(), p_i119_1_.getSpriteWidth(), p_i119_1_.getSpriteHeight(), p_i119_3_, p_i119_4_, p_i119_5_, p_i119_6_));
        this.spriteInfos = p_i119_2_;
    }
}

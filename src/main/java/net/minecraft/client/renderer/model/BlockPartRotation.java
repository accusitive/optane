package net.minecraft.client.renderer.model;

import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Vector3f;

public class BlockPartRotation
{
    public final Vector3f origin;
    public final Direction.Axis axis;
    public final float angle;
    public final boolean rescale;

    public BlockPartRotation(Vector3f p_i1422_1_, Direction.Axis p_i1422_2_, float p_i1422_3_, boolean p_i1422_4_)
    {
        this.origin = p_i1422_1_;
        this.axis = p_i1422_2_;
        this.angle = p_i1422_3_;
        this.rescale = p_i1422_4_;
    }
}

package net.minecraft.client;

import com.mojang.authlib.properties.PropertyMap;
import java.io.File;
import java.net.Proxy;
import javax.annotation.Nullable;
import net.minecraft.client.renderer.ScreenSize;
import net.minecraft.client.resources.FolderResourceIndex;
import net.minecraft.client.resources.ResourceIndex;
import net.minecraft.util.Session;

public class GameConfiguration
{
    public final GameConfiguration.UserInformation userInfo;
    public final ScreenSize displayInfo;
    public final GameConfiguration.FolderInformation folderInfo;
    public final GameConfiguration.GameInformation gameInfo;
    public final GameConfiguration.ServerInformation serverInfo;

    public GameConfiguration(GameConfiguration.UserInformation p_i3708_1_, ScreenSize p_i3708_2_, GameConfiguration.FolderInformation p_i3708_3_, GameConfiguration.GameInformation p_i3708_4_, GameConfiguration.ServerInformation p_i3708_5_)
    {
        this.userInfo = p_i3708_1_;
        this.displayInfo = p_i3708_2_;
        this.folderInfo = p_i3708_3_;
        this.gameInfo = p_i3708_4_;
        this.serverInfo = p_i3708_5_;
    }

    public static class FolderInformation
    {
        public final File gameDir;
        public final File resourcePacksDir;
        public final File assetsDir;
        @Nullable
        public final String assetIndex;

        public FolderInformation(File p_i2011_1_, File p_i2011_2_, File p_i2011_3_, @Nullable String p_i2011_4_)
        {
            this.gameDir = p_i2011_1_;
            this.resourcePacksDir = p_i2011_2_;
            this.assetsDir = p_i2011_3_;
            this.assetIndex = p_i2011_4_;
        }

        public ResourceIndex getAssetsIndex()
        {
            return (ResourceIndex)(this.assetIndex == null ? new FolderResourceIndex(this.assetsDir) : new ResourceIndex(this.assetsDir, this.assetIndex));
        }
    }

    public static class GameInformation
    {
        public final boolean isDemo;
        public final String version;
        public final String versionType;
        public final boolean field_239099_d_;
        public final boolean field_239100_e_;

        public GameInformation(boolean p_i929_1_, String p_i929_2_, String p_i929_3_, boolean p_i929_4_, boolean p_i929_5_)
        {
            this.isDemo = p_i929_1_;
            this.version = p_i929_2_;
            this.versionType = p_i929_3_;
            this.field_239099_d_ = p_i929_4_;
            this.field_239100_e_ = p_i929_5_;
        }
    }

    public static class ServerInformation
    {
        @Nullable
        public final String serverName;
        public final int serverPort;

        public ServerInformation(@Nullable String p_i3135_1_, int p_i3135_2_)
        {
            this.serverName = p_i3135_1_;
            this.serverPort = p_i3135_2_;
        }
    }

    public static class UserInformation
    {
        public final Session session;
        public final PropertyMap userProperties;
        public final PropertyMap profileProperties;
        public final Proxy proxy;

        public UserInformation(Session p_i4157_1_, PropertyMap p_i4157_2_, PropertyMap p_i4157_3_, Proxy p_i4157_4_)
        {
            this.session = p_i4157_1_;
            this.userProperties = p_i4157_2_;
            this.profileProperties = p_i4157_3_;
            this.proxy = p_i4157_4_;
        }
    }
}

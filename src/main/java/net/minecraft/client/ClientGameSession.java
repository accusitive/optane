package net.minecraft.client;

import com.mojang.bridge.game.GameSession;
import java.util.UUID;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.network.play.ClientPlayNetHandler;
import net.minecraft.client.network.play.NetworkPlayerInfo;
import net.minecraft.client.world.ClientWorld;

public class ClientGameSession implements GameSession
{
    private final int playerCount;
    private final boolean remoteServer;
    private final String difficulty;
    private final String gameMode;
    private final UUID sessionId;

    public ClientGameSession(ClientWorld p_i1535_1_, ClientPlayerEntity requestPacket, ClientPlayNetHandler p_i1535_3_)
    {
        this.playerCount = p_i1535_3_.getPlayerInfoMap().size();
        this.remoteServer = !p_i1535_3_.getNetworkManager().isLocalChannel();
        this.difficulty = p_i1535_1_.getDifficulty().getTranslationKey();
        NetworkPlayerInfo networkplayerinfo = p_i1535_3_.getPlayerInfo(requestPacket.getUniqueID());

        if (networkplayerinfo != null)
        {
            this.gameMode = networkplayerinfo.getGameType().getName();
        }
        else
        {
            this.gameMode = "unknown";
        }

        this.sessionId = p_i1535_3_.getSessionId();
    }

    public int getPlayerCount()
    {
        return this.playerCount;
    }

    public boolean isRemoteServer()
    {
        return this.remoteServer;
    }

    public String getDifficulty()
    {
        return this.difficulty;
    }

    public String getGameMode()
    {
        return this.gameMode;
    }

    public UUID getSessionId()
    {
        return this.sessionId;
    }
}

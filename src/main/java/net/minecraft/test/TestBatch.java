package net.minecraft.test;

import java.util.Collection;
import java.util.function.Consumer;
import javax.annotation.Nullable;
import net.minecraft.world.server.ServerWorld;

public class TestBatch
{
    private final String field_229460_a_;
    private final Collection<TestFunctionInfo> field_229461_b_;
    @Nullable
    private final Consumer<ServerWorld> field_229462_c_;

    public TestBatch(String p_i353_1_, Collection<TestFunctionInfo> p_i353_2_, @Nullable Consumer<ServerWorld> p_i353_3_)
    {
        if (p_i353_2_.isEmpty())
        {
            throw new IllegalArgumentException("A GameTestBatch must include at least one TestFunction!");
        }
        else
        {
            this.field_229460_a_ = p_i353_1_;
            this.field_229461_b_ = p_i353_2_;
            this.field_229462_c_ = p_i353_3_;
        }
    }

    public String func_229463_a_()
    {
        return this.field_229460_a_;
    }

    public Collection<TestFunctionInfo> func_229465_b_()
    {
        return this.field_229461_b_;
    }

    public void func_229464_a_(ServerWorld p_229464_1_)
    {
        if (this.field_229462_c_ != null)
        {
            this.field_229462_c_.accept(p_229464_1_);
        }
    }
}

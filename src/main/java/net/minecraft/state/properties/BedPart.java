package net.minecraft.state.properties;

import net.minecraft.util.IStringSerializable;

public enum BedPart implements IStringSerializable
{
    HEAD("head"),
    FOOT("foot");

    private final String name;

    private BedPart(String p_i1918_3_)
    {
        this.name = p_i1918_3_;
    }

    public String toString()
    {
        return this.name;
    }

    public String getName1()
    {
        return this.name;
    }
}

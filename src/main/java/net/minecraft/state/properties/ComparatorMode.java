package net.minecraft.state.properties;

import net.minecraft.util.IStringSerializable;

public enum ComparatorMode implements IStringSerializable
{
    COMPARE("compare"),
    SUBTRACT("subtract");

    private final String name;

    private ComparatorMode(String p_i3546_3_)
    {
        this.name = p_i3546_3_;
    }

    public String toString()
    {
        return this.name;
    }

    public String getName1()
    {
        return this.name;
    }
}

package net.minecraft.state.properties;

import net.minecraft.util.IStringSerializable;

public enum DoubleBlockHalf implements IStringSerializable
{
    UPPER,
    LOWER;

    public String toString()
    {
        return this.getName1();
    }

    public String getName1()
    {
        return this == UPPER ? "upper" : "lower";
    }
}

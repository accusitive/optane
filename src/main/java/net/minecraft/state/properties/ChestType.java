package net.minecraft.state.properties;

import net.minecraft.util.IStringSerializable;

public enum ChestType implements IStringSerializable
{
    SINGLE("single", 0),
    LEFT("left", 2),
    RIGHT("right", 1);

    public static final ChestType[] VALUES = values();
    private final String name;
    private final int opposite;

    private ChestType(String p_i3843_3_, int p_i3843_4_)
    {
        this.name = p_i3843_3_;
        this.opposite = p_i3843_4_;
    }

    public String getName1()
    {
        return this.name;
    }

    public ChestType opposite()
    {
        return VALUES[this.opposite];
    }
}

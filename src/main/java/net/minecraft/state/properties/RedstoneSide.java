package net.minecraft.state.properties;

import net.minecraft.util.IStringSerializable;

public enum RedstoneSide implements IStringSerializable
{
    UP("up"),
    SIDE("side"),
    NONE("none");

    private final String name;

    private RedstoneSide(String p_i899_3_)
    {
        this.name = p_i899_3_;
    }

    public String toString()
    {
        return this.getName1();
    }

    public String getName1()
    {
        return this.name;
    }

    public boolean func_235921_b_()
    {
        return this != NONE;
    }
}

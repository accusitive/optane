package net.minecraft.state.properties;

import net.minecraft.util.IStringSerializable;

public enum DoorHingeSide implements IStringSerializable
{
    LEFT,
    RIGHT;

    public String toString()
    {
        return this.getName1();
    }

    public String getName1()
    {
        return this == LEFT ? "left" : "right";
    }
}

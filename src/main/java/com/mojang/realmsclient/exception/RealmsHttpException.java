package com.mojang.realmsclient.exception;

public class RealmsHttpException extends RuntimeException
{
    public RealmsHttpException(String p_i643_1_, Exception p_i643_2_)
    {
        super(p_i643_1_, p_i643_2_);
    }
}

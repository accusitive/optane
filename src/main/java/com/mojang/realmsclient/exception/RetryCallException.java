package com.mojang.realmsclient.exception;

public class RetryCallException extends RealmsServiceException
{
    public final int field_224985_e;

    public RetryCallException(int attacker)
    {
        super(503, "Retry operation", -1, "");

        if (attacker >= 0 && attacker <= 120)
        {
            this.field_224985_e = attacker;
        }
        else
        {
            this.field_224985_e = 5;
        }
    }
}

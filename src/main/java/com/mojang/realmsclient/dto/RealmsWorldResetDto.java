package com.mojang.realmsclient.dto;

import com.google.gson.annotations.SerializedName;
import net.minecraft.realms.IPersistentSerializable;

public class RealmsWorldResetDto extends ValueObject implements IPersistentSerializable
{
    @SerializedName("seed")
    private final String field_230628_a_;
    @SerializedName("worldTemplateId")
    private final long field_230629_b_;
    @SerializedName("levelType")
    private final int field_230630_c_;
    @SerializedName("generateStructures")
    private final boolean field_230631_d_;

    public RealmsWorldResetDto(String p_i1893_1_, long p_i1893_2_, int p_i1893_4_, boolean p_i1893_5_)
    {
        this.field_230628_a_ = p_i1893_1_;
        this.field_230629_b_ = p_i1893_2_;
        this.field_230630_c_ = p_i1893_4_;
        this.field_230631_d_ = p_i1893_5_;
    }
}
